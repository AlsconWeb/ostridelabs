<?php
/**
 * Template Name: About
 * Template Post Type: page
 *
 * @package OSTD
 */

get_header();

get_template_part( 'template-parts/template', 'about' );

get_footer();

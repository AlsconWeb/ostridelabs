<?php
/**
 * Template Name: Career
 * Template Post Type: page
 *
 * @package OSTD
 */

get_header();

get_template_part( 'template-parts/template', 'career' );

get_footer();

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package OSTD
 */

$logo          = get_field( 'logo_image', url_to_postid( get_home_url() ) );
$contact_phone = get_field( 'contact_phone', url_to_postid( get_home_url() ) );
$copyright     = get_field( 'copyright', url_to_postid( get_home_url() ) );
$contact_email = get_field( 'contact_email', url_to_postid( get_home_url() ) );
?>

<footer class="footer">
	<div class="container">
		<div class="footer__wrap">
			<div class="footer__body">
				<div class="footer__left">
					<a href="<?php echo esc_url( get_home_url() ); ?>" class="footer__logo logo">
						<img src="<?php echo esc_url( $logo ); ?>" alt="logo">
					</a>
					<?php
					if ( function_exists( 'get_custom_menu' ) ) {
						$footer_menu = get_custom_menu( 'footer_menu' );
					}
					?>
					<div class="footer__nav nav">
						<?php
						if ( ! empty( $footer_menu ) ) :
							foreach ( $footer_menu as $footer_menu_item ) :
								?>
								<a href="<?php echo esc_url( $footer_menu_item->url ); ?>" class="nav__link">
									<?php echo esc_html( $footer_menu_item->title ); ?>
								</a>
							<?php
							endforeach;
						endif;
						?>
					</div>
					<div class="footer-social">

						<?php
						$link            = get_field( 'linkedin', 'option' );

						if ( $link ) :
							$link_url = $link['url'];
							$link_title  = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a
									class="footer-social__item" href="<?php echo esc_url( $link_url ); ?>"
									target="<?php echo esc_attr( $link_target ); ?>">
								<img
										src="https://ostridelabs.com/wp-content/uploads/2021/09/linkedin.svg"
										alt="linkedin social link">
							</a>
						<?php
						endif;

						$link            = get_field( 'facebook', 'option' );

						if ( $link ) :
							$link_url = $link['url'];
							$link_title  = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a
									class="footer-social__item" href="<?php echo esc_url( $link_url ); ?>"
									target="<?php echo esc_attr( $link_target ); ?>">
								<img
										src="https://ostridelabs.com/wp-content/uploads/2021/09/facebook.svg"
										alt="facebook social link">
							</a>
						<?php
						endif;
						$link            = get_field( 'Whatsapp', 'option' );

						if ( $link ) :
							$link_url = $link['url'];
							$link_title  = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a
									class="footer-social__item" href="<?php echo esc_url( $link_url ); ?>"
									target="<?php echo esc_attr( $link_target ); ?>">
								<img
										src="https://ostridelabs.com/wp-content/uploads/2021/09/whatsapp-1.svg"
										alt="Whatsapp social link">
							</a>
						<?php
						endif;
						$link            = get_field( 'xing', 'option' );
						if ( $link ) :
							$link_url = $link['url'];
							$link_title  = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a
									class="footer-social__item" href="<?php echo esc_url( $link_url ); ?>"
									target="<?php echo esc_attr( $link_target ); ?>">
								<img
										src="https://ostridelabs.com/wp-content/uploads/2021/09/xing-social-logotype.svg"
										alt="xing social link">
							</a>
						<?php
						endif;
						$link            = get_field( 'Reddit', 'option' );
						if ( $link ) :
							$link_url = $link['url'];
							$link_title  = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a
									class="footer-social__item" href="<?php echo esc_url( $link_url ); ?>"
									target="<?php echo esc_attr( $link_target ); ?>">
								<img
										src="https://ostridelabs.com/wp-content/uploads/2021/09/reddit.svg"
										alt="Reddit social link">
							</a>
						<?php
						endif;
						$link            = get_field( 'GitHub', 'option' );
						if ( $link ) :
							$link_url = $link['url'];
							$link_title  = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a
									class="footer-social__item" href="<?php echo esc_url( $link_url ); ?>"
									target="<?php echo esc_attr( $link_target ); ?>">
								<img
										src="https://ostridelabs.com/wp-content/uploads/2021/09/github-sign.svg"
										alt="GitHub social link">
							</a>
						<?php
						endif;
						$link            = get_field( 'Youtube', 'option' );
						if ( $link ) :
							$link_url = $link['url'];
							$link_title  = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a
									class="footer-social__item" href="<?php echo esc_url( $link_url ); ?>"
									target="<?php echo esc_attr( $link_target ); ?>">
								<img
										src="https://ostridelabs.com/wp-content/uploads/2021/09/youtube.svg"
										alt="youtube social link">
							</a>
						<?php
						endif;
						$link            = get_field( 'medium', 'option' );
						if ( $link ) :
							$link_url = $link['url'];
							$link_title  = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a
									class="footer-social__item" href="<?php echo esc_url( $link_url ); ?>"
									target="<?php echo esc_attr( $link_target ); ?>">
								<img
										src="https://ostridelabs.com/wp-content/uploads/2021/09/medium.svg"
										alt="medium social link">
							</a>
						<?php
						endif;
						$link            = get_field( 'twitter', 'option' );
						if ( $link ) :
							$link_url = $link['url'];
							$link_title  = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a
									class="footer-social__item" href="<?php echo esc_url( $link_url ); ?>"
									target="<?php echo esc_attr( $link_target ); ?>">
								<img
										src="https://ostridelabs.com/wp-content/uploads/2021/11/twitter.svg"
										alt="twitter social link">
							</a>
						<?php endif; ?>
					</div>
				</div>
				<div class="footer__center">
					<?php get_template_part( 'template-parts/block/news-letter' ); ?>
				</div>
				<div class="footer__contact contact-widget">
					<h5 class="heading-sm contact-widget__title"><?php esc_html_e( 'Contact us', 'ostd' ); ?></h5>
					<div class="contact-widget__item">
						<div class="contact-widget__icon">
							<img
									src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/svg/phone-forwarded.svg' ); ?>"
									alt="phone-forwarded">
						</div>
						<a href="tel:<?php echo esc_url( $contact_phone ); ?>" class="text-lg contact-widget__text">
							<?php echo esc_html( $contact_phone ); ?></a>,
						<a href="tel:+44 204 571 7565" class="text-lg contact-widget__text"> +44 204 571 7565</a>
					</div>
					<div class="contact-widget__item">
						<div class="contact-widget__icon">
							<img
									src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/svg/mail2.svg' ); ?>"
									alt="mail"
							>
						</div>
						<a
								href="mailto:<?php echo esc_url( $contact_email ); ?>"
								class="text-lg contact-widget__text contact-widget__text--uline">
							<?php echo esc_html( $contact_email ); ?>
						</a>
					</div>
				</div>
			</div>
			<div class="footer__bottom">
				<p class="footer__terms"><?php echo esc_html( $copyright ); ?></p>
			</div>
		</div>
	</div>
</footer>
<section class="third-bg connect-with d-none">
	<div class="container">
		<div class="connect-with__wrap">
			<h4 class="heading-sm connect-with__title">
				<?php esc_html_e( 'Looking to create value-added services that improve your user satisfaction rates?', 'ostd' ); ?>
			</h4>
			<div class="subtitle subtitle--main connect-with__subtitle">
				<?php esc_html_e( 'Connect with OSTD Labs today to learn more.', 'ostd' ); ?>
			</div>
			<a href="#" class="btn btn--secondary-light btn--md connect-with__btn">
				<span><?php esc_html_e( 'Learn more', 'ostd' ); ?></span>
			</a>
		</div>
	</div>
</section>
<div class="flash flash--load" id="contactStatus">
	<div class="flash__wrap">
		<div class="flash__spinner spinner"></div>
		<p class="flash__text flash__text--success">
			<?php echo wp_kses_post( __( '<strong>Success!</strong> The request has been submitted.', 'ostd' ) ); ?>
		</p>
		<p class="flash__text flash__text--deny">
			<?php echo wp_kses_post( __( '<strong>Oh snap!</strong> You have 2 invalid fields.', 'ostd' ) ); ?>
		</p>
		<p class="flash__text flash__text--neutral">
			<?php echo wp_kses_post( __( "We've sent a text message to your email", 'ostd' ) ); ?>
		</p>
	</div>
</div>
<div class="flash flash--load" id="SubscribeStatus">
	<div class="flash__wrap">
		<div class="flash__close">
			<svg
					version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
					xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="121.31px" height="122.876px"
					viewBox="0 0 121.31 122.876" enable-background="new 0 0 121.31 122.876" xml:space="preserve"><g>
					<path
							fill-rule="evenodd" fill="#EFFFF0" clip-rule="evenodd"
							d="M90.914,5.296c6.927-7.034,18.188-7.065,25.154-0.068 c6.961,6.995,6.991,18.369,0.068,25.397L85.743,61.452l30.425,30.855c6.866,6.978,6.773,18.28-0.208,25.247 c-6.983,6.964-18.21,6.946-25.074-0.031L60.669,86.881L30.395,117.58c-6.927,7.034-18.188,7.065-25.154,0.068 c-6.961-6.995-6.992-18.369-0.068-25.397l30.393-30.827L5.142,30.568c-6.867-6.978-6.773-18.28,0.208-25.247 c6.983-6.963,18.21-6.946,25.074,0.031l30.217,30.643L90.914,5.296L90.914,5.296z"/>
				</g>
			</svg>
		</div>
		<div class="flash__spinner spinner"></div>
		<p class="flash__text flash__text--success">
			<?php esc_html_e( 'Thanks! Please check your inbox.', 'ostd' ); ?>
		</p>
		<p class="flash__text flash__text--deny">
			<?php echo wp_kses_post( __( '<strong>Oh snap!</strong> You have invalid fields.', 'ostd' ) ); ?>
		</p>
		<p class="flash__text flash__text--neutral">
			<?php esc_html_e( "We've sent a text message to your email", 'ostd' ); ?>
		</p>
	</div>
</div>
<div class="popup" id="oppPopup">
	<div class="popup__back" data-closepopup>
	</div>
	<div class="popup__content oppPopup">
		<div class="oppPopup__header">
			<h4 class="subtitle oppPopup__title"></h4>
			<div class="opportunity-item__time oppPopup__time">
				<div class="icon">
					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path
								d="M11.99 2C6.47 2 2 6.48 2 12C2 17.52 6.47 22 11.99 22C17.52 22 22 17.52 22 12C22 6.48 17.52 2 11.99 2ZM12 20C7.58 20 4 16.42 4 12C4 7.58 7.58 4 12 4C16.42 4 20 7.58 20 12C20 16.42 16.42 20 12 20Z"
								fill="#595959"/>
						<path d="M12.5 7H11V13L16.25 16.15L17 14.92L12.5 12.25V7Z" fill="#595959"/>
					</svg>
				</div>
				<p class="text"></p>
			</div>
			<button class="oppPopup__close" data-closepopup></button>
		</div>
		<div class="oppPopup__body">
			<p class="subtitle subtitle--main oppPopup__subtitle"><?php esc_html_e( 'Requirements', 'ostd' ); ?></p>
			<ul class="cust-list oppPopup__list">
			</ul>
		</div>

	</div>
</div>
<?php wp_footer(); ?>

<script>
	window.intercomSettings = {
		api_base: 'https://api-iam.intercom.io',
		app_id: 'pwol2rm8'
	};
</script>

<script>
	// We pre-filled your app ID in the widget URL: 'https://widget.intercom.io/widget/pwol2rm8'
	( function() {
		var w = window;
		var ic = w.Intercom;
		if ( typeof ic === 'function' ) {
			ic( 'reattach_activator' );
			ic( 'update', w.intercomSettings );
		} else {
			var d = document;
			var i = function() {
				i.c( arguments );
			};
			i.q = [];
			i.c = function( args ) {
				i.q.push( args );
			};
			w.Intercom = i;
			var l = function() {
				var s = d.createElement( 'script' );
				s.type = 'text/javascript';
				s.async = true;
				s.src = 'https://widget.intercom.io/widget/pwol2rm8';
				var x = d.getElementsByTagName( 'script' )[ 0 ];
				x.parentNode.insertBefore( s, x );
			};
			if ( document.readyState === 'complete' ) {
				l();
			} else if ( w.attachEvent ) {
				w.attachEvent( 'onload', l );
			} else {
				w.addEventListener( 'load', l, false );
			}
		}
	} )();
</script>
</body>
</html>

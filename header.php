<?php

$logo          = get_field( 'logo_image', url_to_postid( get_home_url() ) );
$contact_phone = get_field( 'contact_phone', url_to_postid( get_home_url() ) );
$contact_email = get_field( 'contact_email', url_to_postid( get_home_url() ) );

?>
<!doctype html>
<html lang="en">
<head>
	<meta name="facebook-domain-verification" content="a2zmmtn1gkoraxd4sycezr4stgfcgc"/>
	<!-- Google Tag Manager -->
	<script>( function( w, d, s, l, i ) {
			w[ l ] = w[ l ] || [];
			w[ l ].push( {
				'gtm.start':
					new Date().getTime(), event: 'gtm.js'
			} );
			var f = d.getElementsByTagName( s )[ 0 ],
				j = d.createElement( s ), dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore( j, f );
		} )( window, document, 'script', 'dataLayer', 'GTM-WX6PCSX' );</script>
	<!-- End Google Tag Manager -->
	<?php

	$ip       = ! empty( $_SERVER['REMOTE_ADDR'] ) ? filter_var( wp_unslash( $_SERVER['REMOTE_ADDR'] ), FILTER_VALIDATE_IP ) : '188.163.51.252';
	$response = wp_remote_get( "http://www.geoplugin.net/json.gp?ip={$ip}" );

	if ( ! is_wp_error( $response ) && 200 === wp_remote_retrieve_response_code( $response ) ) {
		$details = json_decode( wp_remote_retrieve_body( $response ) );
	}

	?>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		var fired = false;

		window.addEventListener( 'scroll', () => {
			if ( fired === false ) {
				fired = true;

				setTimeout( () => {
					( function( m, e, t, r, i, k, a ) {
						m[ i ] = m[ i ] || function() {
							( m[ i ].a = m[ i ].a || [] ).push( arguments )
						};
						m[ i ].l = 1 * new Date();
						k = e.createElement( t ), a = e.getElementsByTagName( t )[ 0 ], k.async = 1, k.src = r, a.parentNode.insertBefore( k, a )
					} )( window, document, 'script', 'https://mc.yandex.ru/metrika/tag.js', 'ym' );
					ym( 81994843, 'init', {
						clickmap: true,
						trackLinks: true,
						accurateTrackBounce: true,
						webvisor: true
					} );
				}, 1000 )
			}
		} );


	</script>
	<noscript>
		<div><img src="https://mc.yandex.ru/watch/81994843" style="position:absolute; left:-9999px;" alt=""/></div>
	</noscript> <!-- /Yandex.Metrika counter -->
	<meta charset="UTF-8">
	<meta
			name="viewport"
			content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link
			rel='shortcut icon' type='image/x-icon'
			href='<?php echo esc_url( get_template_directory_uri() . '/assets/img/favicon.ico' ); ?>'/>
	<?php
	wp_head();
	?>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
	<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WX6PCSX"
			height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<header class="header sticky 123">
	<div class="container">
		<div class="header__wrap">
			<a href="<?php echo esc_url( get_home_url() ); ?>" class="header__logo logo">
				<img src="<?php echo esc_url( $logo ); ?>">
			</a>
			<?php
			if ( function_exists( 'get_custom_menu' ) ) {
				$header_menu = get_custom_menu( 'main_menu' );
			}
			?>
			<nav class="nav header__nav">
				<?php
				if ( ! empty( $header_menu ) ) {
					foreach ( $header_menu as $header_menu_item ) {
						if ( ! empty( $header_menu_item->menu_item_parent ) ) {
							continue;
						}
						$child = true_get_nav_menu_children_items( $header_menu_item->ID, $header_menu );
						if ( $child ) {
							?>
							<div class="nav__link">
								<p><?php echo esc_html( $header_menu_item->title ); ?>
									<span class="nav__link-icon">
										<img
												src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/svg/arrowDropdown.svg' ); ?>"
												alt="arrowDropdown">
									</span>
								</p>
								<ul class="nav__submenu">
									<?php foreach ( $child as $child_item ) : ?>
										<li>
											<a href="<?php echo esc_url( $child_item->url ); ?>"><?php echo esc_html( $child_item->title ); ?></a>
										</li>
									<?php endforeach; ?>
								</ul>
							</div>
							<?php
						} else {
							?>
							<a
									class="nav__link"
									href="<?php echo esc_url( $header_menu_item->url ); ?>">
								<?php echo esc_html( $header_menu_item->title ); ?>
							</a>
							<?php
						}
					}
				}
				?>
			</nav>
			<div class="header__tools">
				<a class="link" href="tel:+44 204 571 7565"><span>+44 204 571 7565</span></a>
				<a href="#contact" class="btn btn--main btn--lg header__btn">
					<span><?php esc_html_e( 'Book a free consultation', 'ostd' ); ?></span>
				</a>
			</div>

			<div class="header__burger burger" id="header-burger">
				<div class="burger__line"></div>
				<div class="burger__line"></div>
				<div class="burger__line"></div>
			</div>
		</div>
	</div>
	<div class="header__mob-menu mob-menu" id="header-mob-menu">
		<nav class="nav header__nav">
			<?php
			if ( ! empty( $header_menu ) ) {
				foreach ( $header_menu as $header_menu_item ) {
					if ( ! empty( $header_menu_item->menu_item_parent ) ) {
						continue;
					}
					$child = true_get_nav_menu_children_items( $header_menu_item->ID, $header_menu );
					if ( $child ) {
						?>
						<div class="nav__link">
							<ul class="nav__submenu">
								<?php
								foreach ( $child as $child_item ) {
									?>
									<li>
										<a href="<?php echo esc_url( $child_item->url ); ?>">
											<?php echo esc_html( $child_item->title ); ?>
										</a>
									</li>
									<?php
								}
								?>
							</ul>
						</div>
						<?php
					} else {
						?>
						<a class="nav__link" href="<?php echo esc_url( $header_menu_item->url ); ?>">
							<?php echo esc_html( $header_menu_item->title ); ?>
						</a>
						<?php
					}
				}
			}
			?>
		</nav>
		<div class="header__contact contact-widget">
			<div class="contact-widget__item">
				<div class="contact-widget__icon">
					<img src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/svg/phone-forwarded.svg' ); ?>">
				</div>
				<a href="tel:<?php echo esc_html( $contact_phone ); ?>>" class="text-lg contact-widget__text">
					<?php echo esc_html( $contact_phone ); ?>
				</a>
			</div>
			<div class="contact-widget__item">
				<div class="contact-widget__icon">
					<img src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/svg/mail2.svg' ); ?>">
				</div>
				<a
						href="mailto:<?php echo esc_html( $contact_email ); ?>"
						class="text-lg contact-widget__text contact-widget__text--uline">
					<?php echo esc_html( $contact_email ); ?>
				</a>
			</div>
		</div>
		<div class="header__tools">
			<a class="link" href="tel:+44 204 571 7565"><span>+44 204 571 7565</span></a>
			<button class="btn btn--main btn--lg header__btn">
				<span><?php esc_html_e( 'Book a free consultation', 'ostd' ); ?></span>
			</button>
		</div>
	</div>
</header>

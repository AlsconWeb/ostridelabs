<?php
/**
 * Theme Init.
 *
 * @package OSTD
 */

namespace OSTD;

/**
 * ThemeInit class file.
 */
class ThemeInit {
	public const OSTD_VERSION = '1.0.0';

	/**
	 * ThemeInit construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Theme Init Hook.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );
	}

	/**
	 * Add Style and Scripts.
	 *
	 * @return void
	 */
	public function add_scripts(): void {
		wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css2?family=Barlow:wght@400;700;800&display=swap', [], '1.0.0' );
		wp_enqueue_style( 'slick-carouse', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', [], '1.8.1' );
		wp_enqueue_style( 'new-style', get_stylesheet_directory_uri() . '/assets/css/style.css', [], self::OSTD_VERSION );

		wp_enqueue_script( 'jquery-3', '//code.jquery.com/jquery-3.5.1.min.js', [], '3.5.1', true );
		wp_enqueue_script( 'slick-carouse', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', [], '1.8.1', true );
		wp_enqueue_script(
			'main',
			get_stylesheet_directory_uri() . '/assets/js/main.js',
			[
				'jquery',
				'slick-carouse',
			],
			'1.8.1',
			true
		);
	}
}

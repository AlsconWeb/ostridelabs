<?php
/**
 * Template Name: Our work
 * Template Post Type: page
 *
 * @package OSTD
 */

get_header();

get_template_part( 'template-parts/template', 'ourwork' );

get_footer();

<?php
/**
 * Template Name: Blog
 * Template Post Type: page
 *
 * @package OSTD
 */

get_header();

get_template_part( 'template-parts/template', 'blog' );

get_footer();

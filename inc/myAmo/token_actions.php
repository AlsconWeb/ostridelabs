<?php

use League\OAuth2\Client\Token\AccessToken;

define( 'TOKEN_FILE', get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'token_info.json' );

function save_token( $access_token ) {
	if (
		isset( $access_token )
		&& isset( $access_token['accessToken'] )
		&& isset( $access_token['refreshToken'] )
		&& isset( $access_token['expires'] )
		&& isset( $access_token['baseDomain'] )
	) {
		$data = [
			'accessToken'  => $access_token['accessToken'],
			'expires'      => $access_token['expires'],
			'refreshToken' => $access_token['refreshToken'],
			'baseDomain'   => $access_token['baseDomain'],
		];

		file_put_contents( TOKEN_FILE, wp_json_encode( $data ) );
	} else {
		exit( 'Invalid access token ' . var_export( $access_token, true ) );
	}
}

/**
 * Get Token.
 *
 * @return AccessToken
 */
function get_token() {
	$access_token = wp_json_decode( file_get_contents( TOKEN_FILE ), true );

	if (
		isset( $access_token )
		&& isset( $access_token['accessToken'] )
		&& isset( $access_token['refreshToken'] )
		&& isset( $access_token['expires'] )
		&& isset( $access_token['baseDomain'] )
	) {
		return new AccessToken(
			[
				'access_token'  => $access_token['accessToken'],
				'refresh_token' => $access_token['refreshToken'],
				'expires'       => $access_token['expires'],
				'baseDomain'    => $access_token['baseDomain'],
			]
		);
	} else {
		exit( 'Invalid access token ' . var_export( $access_token, true ) );
	}
}

<?php

use AmoCRM\Collections\ContactsCollection;
use AmoCRM\Collections\CustomFieldsValuesCollection;
use AmoCRM\Collections\Leads\LeadsCollection;
use AmoCRM\Collections\TagsCollection;
use AmoCRM\Exceptions\AmoCRMApiException;
use AmoCRM\Models\CompanyModel;
use AmoCRM\Models\ContactModel;
use AmoCRM\Models\CustomFieldsValues\MultitextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\MultitextCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\MultitextCustomFieldValueModel;
use AmoCRM\Models\LeadModel;
use AmoCRM\Models\TagModel;
use League\OAuth2\Client\Token\AccessTokenInterface;

include_once get_stylesheet_directory() . '/inc/myAmo/bootstrap.php';

$accessToken = get_token();

$apiClient->setAccessToken( $accessToken )
          ->setAccountBaseDomain( $accessToken->getValues()['baseDomain'] )
          ->onAccessTokenRefresh(
	          function ( AccessTokenInterface $accessToken, string $baseDomain ) {
		          save_token(
			          [
				          'accessToken'  => $accessToken->getToken(),
				          'refreshToken' => $accessToken->getRefreshToken(),
				          'expires'      => $accessToken->getExpires(),
				          'baseDomain'   => $baseDomain,
			          ]
		          );
	          }
          );

$externalData = [
	[
		'name'    => 'I want to book a free consultation',
		'email'   => 'test@test.com',
		'contact' => [
			'first_name' => 'Masha',
		],
		'company' => [
			'name' => 'Asdfg LLC',
		],
		'text'    => 'Some message text',
	],
];

$leadsCollection = new LeadsCollection();

//Создадим модели и заполним ими коллекцию
foreach ( $externalData as $externalLead ) {
	$lead = ( new LeadModel() )
		->setName( $externalLead['name'] )
		->setTags(
			( new TagsCollection() )
				->add(
					( new TagModel() )
						->setName( $externalLead['text'] )
				)
		)
		->setCompany(
			( new CompanyModel() )
				->setName( $externalLead['company']['name'] )
		)
		->setContacts(
			( new ContactsCollection() )
				->add(
					( new ContactModel() )
						->setFirstName( $externalLead['contact']['first_name'] )
						->setCustomFieldsValues(
							( new CustomFieldsValuesCollection() )
								->add(
									( new MultitextCustomFieldValuesModel() )
										->setFieldCode( 'EMAIL' )
										->setValues(
											( new MultitextCustomFieldValueCollection() )
												->add(
													( new MultitextCustomFieldValueModel() )
														->setValue( $externalLead['email'] )
												)
										)
								)
						)
				)
		);


	$leadsCollection->add( $lead );
}

//Создадим сделки
try {
	$addedLeadsCollection = $apiClient->leads()->addComplex( $leadsCollection );
} catch ( AmoCRMApiException $e ) {
	printError( $e );
	die;
}


/** @var LeadModel $addedLead */
foreach ( $addedLeadsCollection as $addedLead ) {
	//Пройдемся по добавленным сделкам и выведем результат
	$leadId    = $addedLead->getId();
	$contactId = $addedLead->getContacts()->first()->getId();
	$companyId = $addedLead->getCompany()->getId();

	$externalRequestIds = $addedLead->getComplexRequestIds();
}

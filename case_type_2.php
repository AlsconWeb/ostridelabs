<?php
/**
 * Template Name: Case type 2
 * Template Post Type: cases
 *
 * @package OSTD
 */

get_header();

get_template_part( 'template-parts/template-case_type_2' );

get_footer();

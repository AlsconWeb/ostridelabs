<?php
/**
 * Template name: Home page
 * Template Post Type: page
 *
 * @package OSTD
 */

get_header();

get_template_part( 'template-parts/template', 'home' );

get_footer();

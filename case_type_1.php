<?php
/**
 * Template Name: Case type 1
 * Template Post Type: cases
 *
 * @package OSTD
 */

get_header();

get_template_part( 'template-parts/template-case_type_1' );

get_footer();

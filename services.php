<?php
/**
 * Template Name: Services
 * Template Post Type: page
 *
 * @package OSTD
 */

get_header();

get_template_part( 'template-parts/template', 'services' );

get_footer();

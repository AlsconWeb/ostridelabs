let currentPopup = null;


/**
 * @sliders initialization
 */
(function(){
    let testimonnials = $('.slick-testimonials');
    let mobSliders = [];
    if(window.innerWidth < 976){
        startTestimonialSlider();
    } else {
      startTestimonialSlider();
    }
    window.addEventListener('resize',function(){
        // if(window.innerWidth < 976){
        //     if(!mobSliders.includes(testimonnials) && testimonnials){
        //         startTestimonialSlider();
        //         mobSliders.push(testimonnials)
        //     }
        // }else{
        //     destroyMobSliders();
        // }
    });
    function startTestimonialSlider(){
        testimonnials.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true,
            arrows : false,
            // autoplay: true,
            autoplaySpeed: 2000,
            dots: true,
            responsive: [
                {
                    breakpoint: 1100,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
            ]
        })
    }
    function destroyMobSliders(){
        mobSliders.forEach(item=>{
            item.slick('unslick');
        });
        mobSliders = [];
    }

})();
/**
 * @Opportunity popup
 */
(function(){
    let oppPopup = document.getElementById('oppPopup');
    if(oppPopup){
        let title = oppPopup.querySelector('.oppPopup__title');
        let time = oppPopup.querySelector('.oppPopup__time');
        let list = oppPopup.querySelector('.oppPopup__list');
        window.addEventListener('click', (e)=>{
            if(e.target.hasAttribute('data-opPopup')){
                oppPopup.classList.add('active');
                getPopupData(e.target.dataset.oppopup);
                currentPopup = oppPopup;
            }
        })
        function getPopupData(id){
            let data = opportunitiesItems[id];
            if(data){
                console.log(data);
                fillPopup({
                    title: data.title,
                    time: data.time,
                    list: data.requirements
                })
            }

        }
        function fillPopup(data){
            let listHTML = "";
            console.log(data.title);
            title.innerHTML = data.title;
            time.innerHTML = data.time;
            data.list.forEach(item =>{
                listHTML += `<li>${item}</li>`
            })

            list.innerHTML = listHTML;
        }
    }

})();
window.addEventListener('click',(e)=>{
    if(e.target.hasAttribute('data-closepopup')){
        console.log(currentPopup);
        currentPopup.classList.remove('active');
    }
});
/**
 * @header mob menu opening
 */
(function() {
    let menuOpened = false;
    let burger = document.getElementById('header-burger');
    let menu = document.getElementById('header-mob-menu');
    burger.addEventListener('click', openMenu);
    function openMenu() {
        document.body.classList.toggle('fixed');
        burger.classList.toggle('active');
        menu.classList.toggle('active');
    }
})();
/**
 * @Toggle Cards initialization
 */
(function() {
    class toggleActive{
        constructor(block){
            this.el = block;
            this.init();
        }
        init(){
            this.el.addEventListener("click", function(e){
                if(e.target.closest('.js-toggleActive')){
                    let el = e.target.closest(".js-toggleActive-el");
                    el.classList.toggle("active");
                }
            })
        }
    }
    let  cloudBlock = document.querySelector(".cloud-block");
    let  teamWidget = document.querySelector(".team-widget");
    let  opportunitiesItems = document.querySelector(".opportunities__items");
    if(cloudBlock){
         new toggleActive(cloudBlock);
    }
    if(teamWidget){
        new toggleActive(teamWidget);
    }
    if(opportunitiesItems){
        new toggleActive(opportunitiesItems)
    }
})();
/**
 * contactForm
 */
$("body").on("submit","form[name='js-form']",function(e){
    e.preventDefault();

    _this = $(this);

    let input = $("input.cust-input__input");
    let check = false;
    input.each(function (){
        let indexOf = $(this).val().indexOf(' ');
        if (!(indexOf > 0 || indexOf < 0)){
            check = true;
            $("#empty-message").show();
        }
    });
    if (check)
        return;

    data = _this.serialize();
    data += "&action=sendform";

    data1 = _this.serialize();
    data1 += "&action=sendlead";

    let contact_status = $("#contactStatus");

    contact_status.addClass("flash--load").addClass("active");
    _this.addClass("contact-form--loading");

    $.ajax({
        url: "/wp-admin/admin-ajax.php",
        data: data,
        datatype: "json",
        type: "post",
        success: function(response){
            console.log(response);
            contact_status.removeClass("flash--load");
            _this.removeClass("contact-form--loading");

            contact_status.addClass('flash--success');
            _this.addClass('contact-form--success');
            setTimeout(()=>{
                _this.trigger('reset');
                _this.removeClass('contact-form--success');
                contact_status.removeClass('flash--success');
                contact_status.removeClass('active');
            },2000)

            $("#empty-message").hide();

            $.ajax({
                url: "/wp-admin/admin-ajax.php",
                data: data1,
                datatype: "json",
                type: "post"
            })

        }
    })
});
/**
 * @Scroll to contact form
 */

(function () {
    $("[href='#contact']").click(function() {
        $([document.documentElement, document.body]).animate({ scrollTop: $("#contact").offset().top}, 2000);
    });
})();
/**
 * @ScrollTo main
 */
(function () {
    $("[data-scrollto]").click(function() {
        const idElScrollTo = $(this).data("scrollto");
        if(idElScrollTo){
            $([document.documentElement, document.body]).animate({ scrollTop: $(`#${idElScrollTo}`).offset().top
            }, 1000);
        }else{
            let text = idElScrollTo.length > 0 ? idElScrollTo + 'Not found on page' : 'Set "id" in data attribute' ;
            console.error(text, $(this))
        }
    });
})();

(function($) {
  $(document).ready(function() {
    $(window).scroll(function(){
      var sticky = $('.sticky'),
          scroll = $(window).scrollTop();

      if (scroll >= 100) sticky.addClass('fixed');
      else sticky.removeClass('fixed');
    });

    $("body").on("submit","form[name='js-news']",function(e){
        e.preventDefault();

        _this = $(this);

        let input = _this.find(("input.cust-input__input"));
        let check = false;
        input.each(function (){
            let indexOf = $(this).val().indexOf(' ');
            if (!(indexOf > 0 || indexOf < 0)){
                check = true;
                $("#empty-message").show();
            }
        });
        if (check)
            return;

        data = _this.serialize();
        data += "&action=sendsubscribe";

        let contact_status = $("#SubscribeStatus");

        contact_status.addClass("flash--load").addClass("active");
        _this.addClass("contact-form--loading");

        $.ajax({
            url: "/wp-admin/admin-ajax.php",
            data: data,
            datatype: "json",
            type: "post",
            success: function(response){
                console.log(response);
                contact_status.removeClass("flash--load");
                _this.removeClass("contact-form--loading");

                contact_status.addClass('flash--success');
                _this.addClass('contact-form--success');
                setTimeout(()=>{
                    _this.trigger('reset');
                    _this.removeClass('contact-form--success');
                    // contact_status.removeClass('flash--success');
                    // contact_status.removeClass('active');
                },2000)

                $("#empty-message").hide();

            }
        })
    });

    $('#SubscribeStatus .flash__close').on('click', function() {
      $('#SubscribeStatus').removeClass('active');
    });
  });
})(jQuery);

<?php
/**
 * Template Name: Case type 3
 * Template Post Type: cases
 *
 * @package OSTD
 */

get_header();

get_template_part( 'template-parts/template-case_type_3' );

get_footer();

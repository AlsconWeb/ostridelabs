<?php
/**
 * OSTD functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package OSTD
 */

use OSTD\ThemeInit;

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'ostd_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ostd_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on OSTD, use a find and replace
		 * to change 'ostd' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'ostd', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			[
				'menu-1' => esc_html__( 'Primary', 'ostd' ),
			]
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			[
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			]
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'ostd_custom_background_args',
				[
					'default-color' => 'ffffff',
					'default-image' => '',
				]
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			[
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			]
		);
	}
endif;
add_action( 'after_setup_theme', 'ostd_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ostd_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ostd_content_width', 640 );
}

add_action( 'after_setup_theme', 'ostd_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ostd_widgets_init() {
	register_sidebar(
		[
			'name'          => esc_html__( 'Sidebar', 'ostd' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'ostd' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		]
	);
}

add_action( 'widgets_init', 'ostd_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ostd_scripts() {
	wp_enqueue_style( 'ostd-style', get_stylesheet_uri(), [], _S_VERSION );
	wp_style_add_data( 'ostd-style', 'rtl', 'replace' );

	wp_enqueue_script( 'ostd-navigation', get_template_directory_uri() . '/js/navigation.js', [], _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'ostd_scripts' );

/**
 * Implement the Custom Header feature.
 */
require __DIR__ . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require __DIR__ . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require __DIR__ . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require __DIR__ . '/inc/customizer.php';


/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require __DIR__ . '/inc/jetpack.php';
}

/**
 * Custom Menu.
 *
 * @param string $menu_id menu id.
 *
 * @return array|false
 * @TODO delete this shite.
 */
function get_custom_menu( $menu_id ) {
	$menu_items = [];
	if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_id ] ) ) {
		$menu       = wp_get_nav_menu_object( $locations[ $menu_id ] );
		$menu_items = wp_get_nav_menu_items( $menu->term_id );
	}

	return $menu_items;
}

/**
 * Get Menu Child item.
 *
 * @param $parent_id
 * @param $nav_menu_items
 * @param $dpth
 *
 * @return array
 * @TODO delete this shite.
 */
function true_get_nav_menu_children_items( $parent_id, $nav_menu_items, $dpth = true ) {
	$dochernie = [];
	foreach ( (array) $nav_menu_items as $nav_item ) {
		if ( (int) $nav_item->menu_item_parent === (int) $parent_id ) {
			$dochernie[] = $nav_item;
		}
	}

	return $dochernie;
}

/**
 * Register menu.
 *
 * @return void
 */
function register_my_menu() {
	register_nav_menu( 'main_menu', __( 'Main Menu' ) );
	register_nav_menu( 'footer_menu', __( 'Footer Menu' ) );
}

add_action( 'init', 'register_my_menu' );

/**
 * Ajax for AmoCRM.
 *
 * @return void
 *
 * @TODO rewrite
 */
function send_lead(): void {
	$data   = $_POST;
	$fields = $data['field'];

	foreach ( $fields as $field_item ) {
		if ( ! isset( $field_item['value'] ) || empty( $field_item['value'] ) ) {
			continue;
		}

		$value = ( is_array( $field_item['value'] ) ) ? implode( ", ", $field_item['value'] ) : $field_item['value'];

		$message .= $field_item['name'] . " - " . $value . "\n";

		$form_data[] = [
			'name'  => $field_item['name'],
			'value' => $value,
		];
	}

	get_template_part(
		'inc/myAmo/creat_lead_ajax',
		null,
		[
			'subject'       => $fields['subject']['value'],
			'full_name'     => $fields['full_name']['value'],
			'company_name'  => $fields['company_name']['value'],
			'email_address' => $fields['email_address']['value'],
			'message'       => $fields['message']['value'],
		]
	);

	wp_send_json_success( [ 'message' => 'Send' ] );
}

add_action( "wp_ajax_nopriv_sendlead", "send_lead" );
add_action( "wp_ajax_sendlead", "send_lead" );


/**
 * Send Form.
 *
 * @return void
 *
 * @TODO rewrite
 */
function send_form() {
	$admin_email = get_field( "email_send_form", url_to_postid( get_home_url() ) );
	$subject     = "Contact form";
	$message     = "";

	$data   = $_POST;
	$fields = $data['field'];


	foreach ( $fields as $field_item ) {
		if ( ! isset( $field_item['value'] ) || empty( $field_item['value'] ) ) {
			continue;
		}

		$value = ( is_array( $field_item['value'] ) ) ? implode( ", ", $field_item['value'] ) : $field_item['value'];

		$message .= $field_item['name'] . " - " . $value . "\n";

		$form_data[] = [
			'name'  => $field_item['name'],
			'value' => $value,
		];
	}

	wp_mail( $admin_email, $subject, $message );

	if ( function_exists( "save_form_data" ) ) {
		save_form_data( "Contact form", $form_data );
	}

	$response['message'] = $message;
	header( "Content-Type: application/json" );
	echo wp_json_encode( $response );
	wp_die();

}

add_action( "wp_ajax_nopriv_sendform", "send_form" );
add_action( "wp_ajax_sendform", "send_form" );

/**
 * sendsubscribe
 *
 * @return void
 *
 * @TODO rewrite
 */
function sendsubscribe() {
	$admin_email = get_field( "email_send_form", url_to_postid( get_home_url() ) );
	$subject     = "Subscribe - ostridelabs.com";
	$message     = "";

	$data   = $_POST;
	$fields = $data['field'];


	foreach ( $fields as $field_item ) {
		if ( ! isset( $field_item['value'] ) || empty( $field_item['value'] ) ) {
			continue;
		}

		$value = ( is_array( $field_item['value'] ) ) ? implode( ", ", $field_item['value'] ) : $field_item['value'];

		$message .= $field_item['name'] . " - " . $value . "\n";

		$form_data[] = [
			'name'  => $field_item['name'],
			'value' => $value,
		];
	}

	wp_mail( $admin_email, $subject, $message );

	if ( function_exists( "save_form_data" ) ) {
		save_form_data( "Contact form", $form_data );
	}

	$response['message'] = 'Thanks! Please check your inbox';
	header( "Content-Type: application/json" );
	echo wp_json_encode( $response );
	wp_die();

}

add_action( "wp_ajax_nopriv_sendsubscribe", "sendsubscribe" );
add_action( "wp_ajax_sendsubscribe", "sendsubscribe" );

/**
 * get_url_page.
 *
 * @param $key
 *
 * @return false|string
 *
 * @TODO delete this shite.
 */
function get_url_page( $key ) {
	$link = false;

	$id = get_id_page( $key );

	if ( $id ) {
		$link = get_page_link( $id );
	}

	return $link;
}

/**
 * Даже не знаю зачем и что это делает !
 *
 * @param $key
 *
 * @return false|int
 *
 * @TODO delete this shite.
 */
function get_id_page( $key ) {
	$id = false;

	$pages = get_pages(
		[
			'meta_key'   => '_wp_page_template',
			'meta_value' => $key . '.php',
		]
	);
	if ( ! empty( $pages ) ) {
		$id = $pages[0]->ID;
	}

	return $id;
}

/**
 * custom_list.
 *
 * @param $atts
 * @param $content
 *
 * @return false|string
 *
 * @TODO delete this shite.
 */
function custom_list( $atts, $content ) {
	$list = explode( "\n", wp_strip_all_tags( $content ) );
	foreach ( $list as $key => $list_item ) {
		if ( empty( trim( $list_item ) ) ) {
			unset( $list[ $key ] );
		}
	}

	$html = "";

	ob_start();
	?>
	<ul class="services-exp-card__list cust-list">
		<?php
		foreach ( $list as $list_item ) {
			?>
			<li><?php echo esc_html( $list_item ); ?></li>
			<?php
		}
		?>
	</ul>

	<?php
	$html = ob_get_contents();
	ob_clean();

	return $html;
}

add_shortcode( "custom_list", "custom_list" );

/**
 * Breadcrumbs.
 *
 * @return void
 *
 * @TODO delete this shite.
 */

//phpcs:disable
function my_breadcrumbs() {
	global $post, $wp_query, $author;

	$prepend = '';
	$before  = '<li>';
	$after   = '</li>';
	$home    = esc_html__( 'Home' );

	$shop_page_id    = false;
	$shop_page       = false;
	$front_page_shop = false;
	if ( defined( 'WOOCOMMERCE_VERSION' ) ) {
		$permalinks      = get_option( 'woocommerce_permalinks' );
		$shop_page_id    = wc_get_page_id( 'shop' );
		$shop_page       = get_post( $shop_page_id );
		$front_page_shop = get_option( 'page_on_front' ) === wc_get_page_id( 'shop' );
	}

	// If permalinks contain the shop page in the URI prepend the breadcrumb with shop.

	if (
		$shop_page_id
		&& $shop_page
		&& strpos( $permalinks['product_base'], '/ ' . $shop_page->post_name ) !== false
		&& get_option( 'page_on_front' ) !== $shop_page_id
	) {
		$prepend = $before . '<a href="' . get_permalink( $shop_page ) . '">' . $shop_page->post_title . '</a> ' . $after;
	}

	if ( ( ! is_home() && ! is_front_page() && ! ( is_post_type_archive() && $front_page_shop ) ) || is_paged() ) {
		echo '<ul class="breadcrumbs list-inline">';

		if ( ! empty( $home ) ) {
			echo $before . '<a class="home" href="' . apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) . '">' . $home . '</a>' . $after;
		}

		if ( is_home() ) {

			echo $before . single_post_title( '', false ) . $after;

		} elseif ( is_search() ) {
			echo $before . esc_html__( 'Search Results for: ' ) . get_search_query() . $after;

		} elseif ( is_category() ) {

			if ( get_option( 'show_on_front' ) === 'page' && get_option( 'page_for_posts' ) ) {
				echo $before . '<a href="' . get_permalink( get_option( 'page_for_posts' ) ) . '">' . get_the_title( get_option( 'page_for_posts', true ) ) . '</a>' . $after;
			}

			$cat_obj = $wp_query->get_queried_object();

			echo $before . single_cat_title( '', false ) . $after;

		} elseif ( is_tax( 'product_cat' ) || is_tax( 'portfolio_cat' ) ) {

			echo $prepend;

			if ( is_tax( 'portfolio_cat' ) ) {
				$post_type = get_post_type_object( 'portfolio' );
				echo $before . '<a href="' . get_post_type_archive_link( 'portfolio' ) . '">' . $post_type->labels->singular_name . '</a>' . $after;
			}
			$current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

			$ancestors = array_reverse( get_ancestors( $current_term->term_id, get_query_var( 'taxonomy' ) ) );

			foreach ( $ancestors as $ancestor ) {
				$ancestor = get_term( $ancestor, get_query_var( 'taxonomy' ) );

				echo $before . '<a href="' . get_term_link( $ancestor->slug, get_query_var( 'taxonomy' ) ) . '">' . esc_html( $ancestor->name ) . '</a>' . $after;
			}

			echo $before . esc_html( $current_term->name ) . $after;

		} elseif ( is_tax( 'product_tag' ) ) {

			$queried_object = $wp_query->get_queried_object();
			echo $prepend . $before . ' ' . esc_html__( 'Products tagged &ldquo;' ) . $queried_object->name . '&rdquo;' . $after;

		} elseif ( is_day() ) {

			echo $before . '<a href="' . get_year_link( get_the_time( 'Y' ) ) . '">' . get_the_time( 'Y' ) . '</a>' . $delimiter . $after;
			echo $before . '<a href="' . get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ) . '">' . get_the_time( 'F' ) . '</a>' . $after;
			echo $before . get_the_time( 'd' ) . $after;

		} elseif ( is_month() ) {

			echo $before . '<a href="' . get_year_link( get_the_time( 'Y' ) ) . '">' . get_the_time( 'Y' ) . '</a>' . $after;
			echo $before . get_the_time( 'F' ) . $after;

		} elseif ( is_year() ) {

			echo $before . get_the_time( 'Y' ) . $after;

		} elseif ( is_post_type_archive( 'product' ) && get_option( 'page_on_front' ) !== $shop_page_id ) {

			$_name = wc_get_page_id( 'shop' ) ? get_the_title( wc_get_page_id( 'shop' ) ) : '';

			if ( ! $_name ) {
				$product_post_type = get_post_type_object( 'product' );
				$_name             = $product_post_type->labels->singular_name;
			}

			if ( is_search() ) {

				echo $before . '<a href="' . get_post_type_archive_link( 'product' ) . '">' . $_name . '</a>' . esc_html__( 'Search results for &ldquo;' ) . get_search_query() . '&rdquo;' . $after;

			} elseif ( is_paged() ) {

				echo $before . '<a href="' . get_post_type_archive_link( 'product' ) . '">' . $_name . '</a>' . $after;

			} else {

				echo $before . $_name . $after;

			}

		} elseif ( is_single() && ! is_attachment() ) {

			if ( 'product' === get_post_type() ) {

				echo $prepend;

				if ( $terms = wc_get_product_terms( $post->ID, 'product_cat', [
					'orderby' => 'parent',
					'order'   => 'DESC',
				] ) ) {
					$main_term = $terms[0];
					$ancestors = get_ancestors( $main_term->term_id, 'product_cat' );
					$ancestors = array_reverse( $ancestors );

					foreach ( $ancestors as $ancestor ) {
						$ancestor = get_term( $ancestor, 'product_cat' );

						if ( ! is_wp_error( $ancestor ) && $ancestor ) {
							echo $before . '<a href="' . get_term_link( $ancestor ) . '">' . $ancestor->name . '</a>' . $after;
						}
					}

					echo $before . '<a href="' . get_term_link( $main_term ) . '">' . $main_term->name . '</a>' . $after;

				}

				echo $before . get_the_title() . $after;

			} elseif ( 'post' !== get_post_type() ) {
				$post_type = get_post_type_object( get_post_type() );
				$slug      = $post_type->rewrite;
				echo $before . '<a href="' . get_post_type_archive_link( get_post_type() ) . '">' . $post_type->labels->singular_name . '</a>' . $after;
				echo $before . get_the_title() . $after;

			} else {

				if ( 'post' === get_post_type() && get_option( 'show_on_front' ) === 'page' && get_option( 'page_for_posts' ) ) {
					echo $before . '<a href="' . get_permalink( get_option( 'page_for_posts' ) ) . '">' . get_the_title( get_option( 'page_for_posts', true ) ) . '</a>' . $after;
				}

				echo $before . '<a href="' . get_url_page( "blog" ) . '">Blog</a>' . $after;

				echo $before . get_the_title() . $after;

			}

		} elseif ( is_404() ) {

			echo $before . esc_html__( '404' ) . $after;

		} elseif ( ! is_single() && ! is_page() && get_post_type() !== 'post' ) {

			$post_type = get_post_type_object( get_post_type() );

			if ( $post_type ) {
				echo $before . $post_type->labels->singular_name . $after;
			}

		} elseif ( is_attachment() ) {

			$parent = get_post( $post->post_parent );
			$cat    = get_the_category( $parent->ID );
			$cat    = $cat[0];
			if ( ( $parents = get_category_parents( $cat, true, $after . $before ) ) && ! is_wp_error( $parents ) ) {
				echo $before . substr( $parents, 0, strlen( $parents ) - strlen( $after . $before ) ) . $after;
			}
			echo $before . '<a href="' . get_permalink( $parent ) . '">' . $parent->post_title . '</a>' . $after;
			echo $before . get_the_title() . $after;

		} elseif ( is_page() && ! $post->post_parent ) {

			echo $before . get_the_title() . $after;

		} elseif ( is_page() && $post->post_parent ) {

			$parent_id   = $post->post_parent;
			$breadcrumbs = [];

			while ( $parent_id ) {
				$page          = get_post( $parent_id );
				$breadcrumbs[] = '<a href="' . get_permalink( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a>';
				$parent_id     = $page->post_parent;
			}

			$breadcrumbs = array_reverse( $breadcrumbs );

			foreach ( $breadcrumbs as $crumb ) {
				echo $before . $crumb . $after;
			}

			echo $before . get_the_title() . $after;

		} elseif ( is_search() ) {

			echo $before . esc_html__( 'Search results for &ldquo;' ) . get_search_query() . '&rdquo;' . $after;

		} elseif ( is_tag() ) {

			echo $before . esc_html__( 'Posts tagged &ldquo;' ) . single_tag_title( '', false ) . '&rdquo;' . $after;

		} elseif ( is_author() ) {

			$userdata = get_userdata( $author );
			echo $before . esc_html__( 'Author:' ) . ' ' . $userdata->display_name . $after;

		}

		if ( get_query_var( 'paged' ) ) {
			echo $before . '&nbsp;(' . esc_html__( 'Page' ) . ' ' . get_query_var( 'paged' ) . ')' . $after;
		}

		echo '</ul>';
	} else {
		if ( is_home() && ! is_front_page() ) {
			echo '<ul class="breadcrumbs list-inline">';

			if ( ! empty( $home ) ) {
				echo $before . '<a class="home" href="' . apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) . '">' . $home . '</a>' . $after;

				echo $before . get_the_title( get_option( 'page_for_posts', true ) ) . $after;
			}

			echo '</ul>';
		}
	}
}

//phpcs:enable

/**
 * Add ShortCode news latter.
 *
 * @param $atts
 *
 * @return false|string
 */
function title_shortcode_fun( $atts ) {

	ob_start();
	$args = [ 'container' => 'false' ];
	get_template_part( 'template-parts/block/news-letter', '', $args );

	return ob_get_clean();

}

add_shortcode( 'news_letter_block', 'title_shortcode_fun' );

require_once __DIR__ . '/vendor/autoload.php';
new ThemeInit();

<?php
/**
 * Header data.
 *
 * @package OSTD
 */

$header_title    = get_field( 'header_title' );
$header_subtitle = get_field( 'header_subtitle' );
$header_image    = get_field( 'header_image' );
$header_btns     = get_field( 'header_btns' );

/** Page data */
$step_block         = get_field( 'step_block' );
$partners_block     = get_field( 'partners_block' );
$info_block         = get_field( 'info_block' );
$services_block     = get_field( 'services_block' );
$solutions_block    = get_field( 'solutions_block' );
$testimonials_block = get_field( 'testimonials_block' );
$info_block_2       = get_field( 'info_block_2' );
?>
<main class="main">
	<section class="title-block title-block--home">
		<div class="container container--xxl">
			<div class="title-block__wrap">
				<h1 class="heading title-block__title">
					<?php echo esc_html( $header_title ); ?>
				</h1>
				<div class="title-block__desc">
					<p>
						<?php echo esc_html( $header_subtitle ); ?>
					</p>
				</div>
				<div class="title-block__btns">
					<?php
					if ( $header_btns ) :
						foreach ( $header_btns as $btn_item ) {
							if ( $btn_item['enable'] ) {
								switch ( $btn_item['type'] ) {
									case 'type1':
										?>
										<a
												href="<?php echo esc_url( $btn_item['btn_link'] ); ?>"
												class="btn btn--main btn--lg title-block__btn">
											<span><?php echo esc_html( $btn_item['btn_text'] ); ?></span>
										</a>
										<?php
										break;
									case 'type2':
										?>
										<a
												href="<?php echo esc_url( $btn_item['btn_link'] ); ?>"
												class="btn btn--secondary btn--lg title-block__btn"
												data-scrollto="cloud-block">
											<span><?php echo esc_html( $btn_item['btn_text'] ); ?></span>
										</a>
										<?php
										break;
								}
							}
						}
					endif;
					?>
				</div>
				<div class="title-block__img">
					<img
							class="desk" src="<?php echo esc_url( $header_image['desktop'] ); ?>"
							alt="title-block-img-desk">
					<img
							class="mobile" src="<?php echo esc_url( $header_image['mobile'] ); ?>"
							alt="title-block-img-mobile">

				</div>
			</div>
		</div>
	</section>
	<section class="cloud-block" id="cloud-block">

		<div class="container">
			<div class="cloud-block__wrap">
				<h2 class="heading-md cloud-block__title">
					<?php echo esc_html( $step_block['title'] ); ?>
				</h2>
				<div class="cloud-block__steps cloud-steps">
					<div class="container container--md">
						<h3 class="subtitle subtitle--main cloud-steps__title">
							<?php echo esc_html( $step_block['sub_title'] ); ?>
						</h3>
						<div class="cloud-steps__wrap">
							<div class="cloud-steps__start">
								<div class="arrow"></div>
							</div>
							<div class="cloud-steps__item cloud-item left first js-toggleActive-el">
								<button class="cloud-item__open js-toggleActive"></button>
								<div class="cloud-item__wrap">
									<div class="cloud-item__icon">
										<?php echo $step_block['steps']['step_1']['image']; ?>
									</div>
									<div class="cloud-item__content">

										<?php
										$items     = $step_block['steps']['step_1']['items'];
										$main_item = [];
										foreach ( $items as $key => $value ) {
											if ( $value['main_item'] ) {
												$main_item = $value;
												unset( $items[ $key ] );
											}
										}
										?>

										<div class="cloud-item__item">
											<h3 class="cloud-item__title"><?php echo esc_html( $main_item['title'] ); ?></h3>
											<div class="cloud-item__desc">
												<p><?php echo esc_html( $main_item['text'] ); ?></p>
											</div>
										</div>
										<div class="cloud-item__hidden">
											<?php
											foreach ( $items as $item ) {
												?>
												<div class="cloud-item__item">
													<h3 class="cloud-item__title"><?php echo esc_html( $item['title'] ); ?></h3>
													<div class="cloud-item__desc">
														<p><?php echo esc_html( $item['text'] ); ?></p>
													</div>
												</div>
												<?php
											}
											?>

										</div>
									</div>
								</div>
								<div class="arrow">
									<svg
											width="12" height="21" viewBox="0 0 12 21" fill="none"
											xmlns="http://www.w3.org/2000/svg">
										<path
												d="M12 11.2857L2.32403 20.2299C1.43818 21.0487 0.128519 19.889 0.834167 18.9106L6.33333 11.2857L0.556188 2.47435C-0.106291 1.46393 1.25824 0.363422 2.10544 1.22486L12 11.2857Z"
												fill="#BFE8FF"/>
									</svg>
								</div>
							</div>
							<div class="cloud-steps__item cloud-item right js-toggleActive-el">
								<button class="cloud-item__open js-toggleActive"></button>
								<div class="cloud-item__wrap">
									<div class="cloud-item__icon">
										<?php echo $step_block['steps']['step_2']['image']; ?>
									</div>
									<div class="cloud-item__content">

										<?php
										$items     = $step_block['steps']['step_2']['items'];
										$main_item = [];
										foreach ( $items as $key => $value ) {
											if ( $value['main_item'] ) {
												$main_item = $value;
												unset( $items[ $key ] );
											}
										}
										?>

										<div class="cloud-item__item">
											<h3 class="cloud-item__title"><?php echo esc_html( $main_item['title'] ); ?></h3>
											<div class="cloud-item__desc">
												<p><?php echo esc_html( $main_item['text'] ); ?></p>
											</div>
										</div>
										<div class="cloud-item__hidden">
											<?php
											foreach ( $items as $item ) {
												?>
												<div class="cloud-item__item">
													<h3 class="cloud-item__title"><?php echo esc_html( $item['title'] ); ?></h3>
													<div class="cloud-item__desc">
														<p><?php echo esc_html( $item['text'] ); ?></p>
													</div>
												</div>
												<?php
											}
											?>

										</div>
									</div>
								</div>
								<div class="arrow">
									<svg
											width="12" height="21" viewBox="0 0 12 21" fill="none"
											xmlns="http://www.w3.org/2000/svg">
										<path
												d="M12 11.2857L2.32403 20.2299C1.43818 21.0487 0.128519 19.889 0.834167 18.9106L6.33333 11.2857L0.556188 2.47435C-0.106291 1.46393 1.25824 0.363422 2.10544 1.22486L12 11.2857Z"
												fill="#BFE8FF"/>
									</svg>
								</div>
							</div>
							<div class="cloud-steps__item cloud-item left js-toggleActive-el">
								<button class="cloud-item__open js-toggleActive"></button>
								<div class="cloud-item__wrap ">
									<div class="cloud-item__icon">
										<?php echo $step_block['steps']['step_3']['image']; ?>
									</div>
									<div class="cloud-item__content">

										<?php
										$items     = $step_block['steps']['step_3']['items'];
										$main_item = [];
										foreach ( $items as $key => $value ) {
											if ( $value['main_item'] ) {
												$main_item = $value;
												unset( $items[ $key ] );
											}
										}
										?>

										<div class="cloud-item__item">
											<h3 class="cloud-item__title"><?php echo esc_html( $main_item['title'] ); ?></h3>
											<div class="cloud-item__desc">
												<p><?php echo esc_html( $main_item['text'] ); ?></p>
											</div>
										</div>
										<div class="cloud-item__hidden">
											<?php
											foreach ( $items as $item ) {
												?>
												<div class="cloud-item__item">
													<h3 class="cloud-item__title"><?php echo esc_html( $item['title'] ); ?></h3>
													<div class="cloud-item__desc">
														<p><?php echo esc_html( $item['text'] ); ?></p>
													</div>
												</div>
												<?php
											}
											?>

										</div>
									</div>
								</div>
								<div class="arrow">
									<svg
											width="12" height="21" viewBox="0 0 12 21" fill="none"
											xmlns="http://www.w3.org/2000/svg">
										<path
												d="M12 11.2857L2.32403 20.2299C1.43818 21.0487 0.128519 19.889 0.834167 18.9106L6.33333 11.2857L0.556188 2.47435C-0.106291 1.46393 1.25824 0.363422 2.10544 1.22486L12 11.2857Z"
												fill="#BFE8FF"/>
									</svg>
								</div>
							</div>
							<div class="cloud-steps__item cloud-item right js-toggleActive-el">
								<button class="cloud-item__open js-toggleActive"></button>
								<div class="cloud-item__wrap">
									<div class="cloud-item__icon">
										<?php echo $step_block['steps']['step_4']['image']; ?>
									</div>
									<div class="cloud-item__content">

										<?php
										$items     = $step_block['steps']['step_4']['items'];
										$main_item = [];
										foreach ( $items as $key => $value ) {
											if ( $value['main_item'] ) {
												$main_item = $value;
												unset( $items[ $key ] );
											}
										}
										?>

										<div class="cloud-item__item">
											<h3 class="cloud-item__title"><?php echo esc_html( $main_item['title'] ); ?></h3>
											<div class="cloud-item__desc">
												<p><?php echo esc_html( $main_item['text'] ); ?></p>
											</div>
										</div>
										<div class="cloud-item__hidden">
											<?php
											foreach ( $items as $item ) {
												?>
												<div class="cloud-item__item">
													<h3 class="cloud-item__title"><?php echo esc_html( $item['title'] ); ?></h3>
													<div class="cloud-item__desc">
														<p><?php echo esc_html( $item['text'] ); ?></p>
													</div>
												</div>
												<?php
											}
											?>

										</div>
									</div>
								</div>
								<div class="arrow">
									<svg
											width="12" height="21" viewBox="0 0 12 21" fill="none"
											xmlns="http://www.w3.org/2000/svg">
										<path
												d="M12 11.2857L2.32403 20.2299C1.43818 21.0487 0.128519 19.889 0.834167 18.9106L6.33333 11.2857L0.556188 2.47435C-0.106291 1.46393 1.25824 0.363422 2.10544 1.22486L12 11.2857Z"
												fill="#BFE8FF"/>
									</svg>
								</div>
							</div>
							<div class="cloud-steps__item cloud-item last js-toggleActive-el">
								<button class="cloud-item__open js-toggleActive"></button>
								<div class="cloud-item__wrap">
									<div class="cloud-item__icon">
										<?php echo $step_block['steps']['step_5']['image']; ?>
									</div>
									<div class="cloud-item__content">

										<?php
										$items     = $step_block['steps']['step_5']['items'];
										$main_item = [];
										foreach ( $items as $key => $value ) {
											if ( $value['main_item'] ) {
												$main_item = $value;
												unset( $items[ $key ] );
											}
										}
										?>

										<div class="cloud-item__item">
											<h3 class="cloud-item__title"><?php echo esc_html( $main_item['title'] ); ?></h3>
											<div class="cloud-item__desc">
												<p><?php echo esc_html( $main_item['text'] ); ?></p>
											</div>
										</div>
										<div class="cloud-item__hidden">
											<?php
											foreach ( $items as $item ) {
												?>
												<div class="cloud-item__item">
													<h3 class="cloud-item__title"><?php echo esc_html( $item['title'] ); ?></h3>
													<div class="cloud-item__desc">
														<p><?php echo esc_html( $item['text'] ); ?></p>
													</div>
												</div>
												<?php
											}
											?>

										</div>
									</div>
								</div>
								<div class="arrow">
									<svg
											width="12" height="21" viewBox="0 0 12 21" fill="none"
											xmlns="http://www.w3.org/2000/svg">
										<path
												d="M12 11.2857L2.32403 20.2299C1.43818 21.0487 0.128519 19.889 0.834167 18.9106L6.33333 11.2857L0.556188 2.47435C-0.106291 1.46393 1.25824 0.363422 2.10544 1.22486L12 11.2857Z"
												fill="#BFE8FF"/>
									</svg>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="partners">
		<div class="container">
			<h5 class="partners__title"><?php echo esc_html( $partners_block['title'] ); ?></h5>
			<div class="partners__wrap">
				<?php
				foreach ( $partners_block['logos'] as $logo_item ) {
					?>
					<div class="partners__item">
						<img src="<?php echo esc_url( $logo_item['image'] ); ?>">
					</div>
					<?php
				}
				?>
			</div>
		</div>
	</section>
	<section class="info-block info-block--second">
		<div class="container container--xxl">
			<div class="info-block__wrap">
				<div class="info-block__content ">
					<h2 class="heading-lg info-block__title">
						<?php echo esc_html( $info_block['title'] ); ?>
					</h2>
					<div class="subtitle subtitle--main info-block__desc">
						<p><?php echo esc_html( $info_block['text'] ); ?></p>
					</div>
					<?php
					if ( $info_block['btn']['enable'] ) {
						?>
						<a href="#contact" class="btn btn--main btn--lg info-block__btn">
							<span><?php echo esc_html( $info_block['btn']['btn_text'] ); ?></span>
						</a>
						<?php
					}
					?>
				</div>
				<div class="info-block__img">
					<img src="<?php echo esc_url( $info_block['image'] ); ?>" alt="Group 748.png">
				</div>
				<?php
				if ( $info_block['btn']['enable'] ) {
					?>
					<a href="#contact" class="btn btn--main btn--lg info-block__btn info-block__btn--mobile">
						<span><?php echo esc_html( $info_block['btn']['btn_text'] ); ?></span>
					</a>
					<?php
				}
				?>
			</div>
		</div>
	</section>
	<section class="services-widget main-bg">
		<div class="container">
			<div class="services-widget__wrap">
				<h2 class="heading-lg services-widget__title">
					<?php echo esc_html( $services_block['title'] ); ?>
				</h2>
				<div class="subtitle subtitle--main services-widget__subtitle">
					<p><?php echo esc_html( $services_block['sub_title'] ); ?></p></div>
				<div class="services-widget__cards">
					<?php
					if ( $services_block['service_items'] ) :
						foreach ( $services_block['service_items'] as $service_item ) {
							?>
							<a href="<?php echo esc_url( $service_item['link'] ); ?>">
								<div class="services-widget__card service-card">
									<div class="service-card__icon">
										<img
												src="<?php echo esc_url( $service_item['image'] ); ?>"
												alt="service-card-icon">
									</div>
									<h3 class="subtitle service-card__title"><?php echo esc_html( $service_item['title'] ); ?></h3>
									<div class="body-text service-card__desc">
										<p>
											<?php echo esc_html( $service_item['text'] ); ?>
										</p>
									</div>
									<div
											class="service-card__bottom-bar"
											style="background-color: <?php echo esc_attr( $service_item['color_background'] ); ?>;"></div>
								</div>
							</a>
							<?php
						}
					endif;
					?>
				</div>
			</div>
		</div>
	</section>
	<section class="solution-widget">
		<div class="container">
			<div class="solution-widget__wrap">
				<h2><?php esc_html_e( 'How we work', 'ostd' ); ?></h2>
				<div class="subtitle subtitle--main solution-widget__subtitle">
					<p><?php echo esc_html( $solutions_block['title'] ); ?></p>
					<p>
						<?php echo esc_html( $solutions_block['subtitle'] ); ?>
					</p>
				</div>
				<div class="solution-widget__steps">
					<div class="solution-widget__col">
						<div class="solution-widget__item solution-widget-card">
							<p class="solution-widget-card__number">01</p>
							<div class="solution-widget-card__content">
								<h3 class="subtitle solution-widget-card__title"><?php echo esc_html( $solutions_block['steps']['step_1']['title'] ); ?></h3>
								<div class="body-text solution-widget-card__text">
									<p>
										<?php echo esc_html( $solutions_block['steps']['step_1']['text'] ); ?>
									</p>
								</div>
							</div>
						</div>
						<div class="solution-widget__item solution-widget__item--mobile solution-widget-card">
							<div class="solution-widget-card__top-line"></div>
							<div class="solution-widget-card__bottom-line"></div>
							<p class="solution-widget-card__number">02</p>
							<div class="solution-widget-card__content">
								<h3 class="subtitle solution-widget-card__title"><?php echo esc_html( $solutions_block['steps']['step_2']['title'] ); ?></h3>
								<div class="body-text solution-widget-card__text">
									<p>
										<?php echo esc_html( $solutions_block['steps']['step_2']['text'] ); ?>
									</p>
								</div>
							</div>
						</div>
						<div class="solution-widget__item solution-widget-card solution-widget-card--last">
							<div class="solution-widget-card__line"></div>
							<p class="solution-widget-card__number">03</p>
							<div class="solution-widget-card__content">
								<h3 class="subtitle solution-widget-card__title"><?php echo esc_html( $solutions_block['steps']['step_3']['title'] ); ?></h3>
								<div class="body-text solution-widget-card__text">
									<p>
										<?php echo esc_html( $solutions_block['steps']['step_3']['text'] ); ?>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="solution-widget__col">
						<div class="solution-widget__item solution-widget-card solution-widget-card--reverse">
							<div class="solution-widget-card__top-line"></div>
							<div class="solution-widget-card__bottom-line"></div>
							<p class="solution-widget-card__number">02</p>
							<div class="solution-widget-card__content">
								<h3 class="subtitle solution-widget-card__title"><?php echo esc_html( $solutions_block['steps']['step_2']['title'] ); ?></h3>
								<div class="body-text solution-widget-card__text">
									<p>
										<?php echo esc_html( $solutions_block['steps']['step_2']['text'] ); ?>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php get_template_part( 'template-parts/block/news-letter' ); ?>
	<section class="testimonials second-bg">
		<div class="container">
			<div class="testimonials__wrap">
				<h2 class="heading-lg testimonials__title"><?php echo esc_html( $testimonials_block['title'] ); ?></h2>
				<div class="subtitle subtitle--main testimonials__desc">
					<p><?php echo esc_html( $testimonials_block['subtitle'] ); ?></p>
				</div>
				<div class="testimonials__items slick-testimonials">
					<?php
					if ( $testimonials_block['testimonial_items'] ) :
						foreach ( $testimonials_block['testimonial_items'] as $testimonal_item ) {
							?>
							<div class="testimonials__item testimonial-card">
								<div class="testimonial-card__head">
									<div class="testimonial-card__avatar">
										<img
												src="<?php echo esc_url( $testimonal_item['testimonial']['photo'] ); ?>"
												alt="testimonial-card-avatar">
									</div>
									<div class="testimonial-card__info">
										<h3 class="subtitle testimonial-card__company"><?php echo esc_html( $testimonal_item['testimonial']['name'] ); ?></h3>
										<p class="testimonial-card__name"><?php echo esc_html( $testimonal_item['testimonial']['position'] ); ?></p>
									</div>
								</div>
								<div class="body-text testimonial-card__content">
									<p>
										<?php echo esc_html( $testimonal_item['testimonial']['testimonial_text'] ); ?>
									</p>
								</div>
							</div>
							<?php
						}
					endif;
					?>
				</div>
			</div>
		</div>
	</section>
	<section class="cases main-bg">
		<div class="container">
			<div class="cases__wrap">
				<h2 class="heading-lg cases__title">
					<?php the_field( 'our_cases_title' ); ?>
				</h2>
				<?php
				$cases = new WP_Query(
					[
						'post_type'      => 'cases',
						'posts_per_page' => 3,
					]
				);
				?>
				<div class="cases__items">
					<?php
					if ( $cases->have_posts() ) {
						while ( $cases->have_posts() ) {
							$cases->the_post();
							?>
							<div class="cases__item case-item">
								<a href="<?php the_permalink(); ?>" class="case-item__img">
									<img src="<?php echo esc_url( get_the_post_thumbnail_url() ); ?>">
								</a>
								<h4 class="heading-sm case-item__title"><?php the_title(); ?></h4>
								<div class="body-text case-item__desc">
									<p>
										<?php echo esc_html( get_field( 'except' ) ); ?>
									</p>
								</div>
								<a href="<?php the_permalink(); ?>" class="btn btn--secondary case-item__btn">
									<span><?php esc_html_e( 'Learn more', 'ostd' ); ?></span>
								</a>
							</div>
							<?php
						}
						wp_reset_postdata();
					}
					?>
				</div>
			</div>
		</div>
	</section>
	<section class="info-block">
		<div class="container container--xxl">
			<div class="info-block__wrap">
				<div class="info-block__content">
					<h2 class="heading-lg info-block__title">
						<?php echo esc_html( $info_block_2['title'] ); ?>
					</h2>
					<div class="subtitle subtitle--main info-block__desc">
						<p>
							<?php echo esc_html( $info_block_2['text'] ); ?>
						</p>
					</div>
					<?php
					if ( $info_block_2['btn']['enable'] ) {
						?>
						<a href="#contact" class="btn btn--main btn--md info-block__btn">
							<span><?php echo esc_html( $info_block_2['btn']['btn_text'] ); ?></span>
						</a>
						<?php
					}
					?>
				</div>
				<div class="info-block__img">
					<img src="<?php echo esc_url( $info_block_2['image'] ); ?>" alt="Group 748.png">
				</div>
				<?php
				if ( $info_block_2['btn']['enable'] ) {
					?>
					<a href="#contact" class="btn btn--main btn--lg info-block__btn info-block__btn--mobile">
						<span><?php echo esc_html( $info_block_2['btn']['btn_text'] ); ?></span>
					</a>
					<?php
				}
				?>
			</div>
		</div>
	</section>
	<?php
	get_template_part( 'template-parts/part-contact_form' );
	?>
</main>

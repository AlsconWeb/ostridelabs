<?php
/**
 * Header data
 *
 * @package OSTD
 */

$header_title    = get_field( 'header_title' );
$header_subtitle = get_field( 'header_subtitle' );
$header_image    = get_field( 'header_image' );
$header_btns     = get_field( 'header_btns' );

/** Page data */
$partners_block        = get_field( 'partners_block' );
$drives_block          = get_field( 'drives_block' );
$transform_block       = get_field( 'transform_block' );
$leadership_team_block = get_field( 'leadership_team_block' );
?>
<main class="main">
	<section class="title-block title-block--third about__title-block">
		<div class="container">
			<div class="title-block__wrap">
				<?php my_breadcrumbs(); ?>
				<h1 class="heading title-block__title">
					<?php echo esc_html( $header_title ); ?>
				</h1>
				<div class="subtitle subtitle--main title-block__subtitle">
					<p>
						<?php echo esc_html( $header_subtitle ); ?>
					</p>
				</div>
				<div class="title-block__btns">
					<?php
					if ( ! empty( $header_btns ) ) :
						foreach ( $header_btns as $btn_item ) {
							if ( $btn_item['enable'] ) {
								switch ( $btn_item['type'] ) {
									case 'type1':
										?>
										<a
												href="<?php echo esc_url( $btn_item['btn_link'] ); ?>"
												class="btn btn--main btn--lg title-block__btn">
											<span><?php echo esc_html( $btn_item['btn_text'] ); ?></span>
										</a>
										<?php
										break;
									case 'type2':
										?>
										<a
												href="<?php echo esc_url( $btn_item['btn_link'] ); ?>"
												class="btn btn--secondary btn--lg title-block__btn">
											<span><?php echo esc_html( $btn_item['btn_text'] ); ?></span>
										</a>
										<?php
										break;
								}
							}
						}
					endif;
					?>
				</div>
				<div class="title-block__img ">
					<img
							class="desk" src="<?php echo esc_url( $header_image['desktop'] ); ?>"
							alt="title-block-img-desk">
					<img
							class="mobile"
							src="<?php echo esc_url( $header_image['mobile'] ); ?>"
							alt="title-block-img-mobile">
				</div>

			</div>
		</div>
	</section>
	<section class="partners">
		<div class="container">
			<h5 class="partners__title"><?php echo esc_html( $partners_block['title'] ); ?></h5>
			<div class="partners__wrap">
				<?php
				if ( ! empty( $partners_block['logos'] ) ) :
					foreach ( $partners_block['logos'] as $partner_item ) {
						?>
						<div class="partners__item">
							<img src="<?php echo esc_url( $partner_item['image'] ); ?>" alt="partners-item-img">
						</div>
						<?php
					}
				endif;
				?>
			</div>
		</div>
	</section>
	<section class="drives">
		<div class="container">
			<div class="drives__wrap">
				<h2 class="heading-lg drives__title"><?php echo esc_html( $drives_block['title'] ); ?></h2>
				<div class="icons-table">
					<?php
					if ( ! empty( $drives_block['items'] ) ) :
						foreach ( $drives_block['items'] as $drive_item ) {
							?>
							<div class="icons-table__item">
								<div class="icons-table__icon">
									<img src="<?php echo esc_url( $drive_item['image'] ); ?>" alt="icon">
								</div>
								<div class="icons-table__content">
									<h3 class="heading-sm icons-table__title"><?php echo esc_html( $drive_item['title'] ); ?></h3>
									<div class="body-text icons-table__desc">
										<p>
											<?php echo esc_html( $drive_item['text'] ); ?>
										</p>
									</div>
								</div>
							</div>
							<?php
						}
					endif;
					?>
				</div>
			</div>
		</div>
	</section>
	<section class="bTransform third-bg">
		<div class="container">
			<h2 class="heading-lg bTransform__title"><?php echo esc_html( $transform_block['title'] ); ?></h2>
			<div class="subtitle subtitle--main bTransform__subtitle">
				<p>
					<?php echo esc_html( $transform_block['subtitle'] ); ?>
				</p>
			</div>
			<div class="icons-table">
				<?php
				if ( ! empty( $transform_block['items'] ) ) :
					foreach ( $transform_block['items'] as $transform_item ) {
						?>
						<div class="icons-table__item">
							<div class="icons-table__icon">
								<img src="<?php echo esc_url( $transform_item['image'] ); ?>" alt="icon">
							</div>
							<div class="icons-table__content">
								<h3 class="subtitle subtitle--main icons-table__title"><?php echo esc_html( $transform_item['title'] ); ?></h3>
								<div class="body-text icons-table__desc">
									<p>
										<?php echo esc_html( $transform_item['text'] ); ?>
									</p>
								</div>
							</div>
						</div>
						<?php
					}
				endif;
				?>
			</div>
		</div>
	</section>
	<section class="team-widget">
		<div class="container">
			<div class="team-widget__wrap">
				<h2 class="heading-lg team-widget__title"><?php echo esc_html( $leadership_team_block['title'] ); ?></h2>
				<div class="subtitle subtitle--main team-widget__subtitle">
					<p>
						<?php echo esc_html( $leadership_team_block['subtitle'] ); ?>
					</p>
				</div>
				<div class="team-widget__team">
					<?php
					if ( ! empty( $leadership_team_block['team'] ) ) :
						foreach ( $leadership_team_block['team'] as $team_item ) {
							?>
							<div class="team-widget__card team-card js-toggleActive-el">
								<button class="team-card__open js-toggleActive">
									<svg
											width="10" height="12" viewBox="0 0 10 12" fill="none"
											xmlns="http://www.w3.org/2000/svg">
										<path
												d="M1 7L5 11L9 7" stroke="currentColor" stroke-width="1.67"
												stroke-linecap="round" stroke-linejoin="round"/>
										<path
												d="M5 1V11" stroke="currentColor" stroke-width="1.67"
												stroke-linecap="round"
												stroke-linejoin="round"/>
									</svg>
								</button>
								<div class="team-card__avatar">
									<img src="<?php echo esc_url( $team_item['photo'] ); ?>" alt="team-card-avatar">
								</div>
								<h3 class="heading-xs team-card__name"><?php echo esc_html( $team_item['name'] ); ?></h3>
								<p class="body-text team-card__rank"><?php echo esc_html( $team_item['position'] ); ?></p>
								<div class="team-card__hidden-info team-card-hidden">
									<div class="body-text team-card-hidden__desc">
										<p>
											<?php echo esc_html( $team_item['text'] ); ?>
										</p>
									</div>
									<div class="team-card-hidden__footer">
										<div class="team-card-hidden__info">
											<h3 class="heading-xs team-card-hidden__name"><?php echo esc_html( $team_item['name'] ); ?></h3>
											<p class="body-text team-card-hidden__rank"><?php echo esc_html( $team_item['position'] ); ?></p>
										</div>
										<div class="team-card-hidden__avatar">
											<img
													src="<?php echo esc_url( $team_item['photo'] ); ?>"
													alt="team-card-hidden-avatar">
										</div>
									</div>
								</div>
							</div>
							<?php
						}
					endif;
					?>
				</div>
			</div>
		</div>
	</section>
	<?php
	get_template_part( 'template-parts/part-contact_form' );
	?>
</main>

<?php
$header_title      = get_field( 'header_title' );
$header_subtitle   = get_field( 'header_subtitle' );
$header_image      = get_field( 'header_image' );
$background_image  = get_field( 'backgroud_image' );
$download_file     = get_field( 'download_file' );
$testimonial_block = get_field( 'testimonial_block' );
$cases             = get_field( 'cases' );
$about_client      = get_field( 'about_client' );
$project_specs     = get_field( 'project_specs' );
$header_btns       = get_field( 'header_btns' );
$services_block    = get_field( 'services_block', get_id_page( 'home' ) );
?>
<main class="main">
	<section class="title-block case-temp__title-block title-block--third gradient-bg">
		<?php
		if ( ! empty( $background_image ) ) {
			?>
			<div class="title-block__img">
				<img src="<?php echo esc_url( $background_image ); ?>">
			</div>
			<?php
		}
		?>
		<div class="container">
			<div class="title-block__wrap">
				<h1 class="heading-lg title-block__title">
					<?php echo esc_html( $header_title ); ?>
				</h1>
				<div class="subtitle subtitle--main title-block__subtitle">
					<p>
						<?php echo esc_html( $header_subtitle ); ?>
					</p>
				</div>
				<div class="home-title-block__btns">
					<?php
					if ( ! empty( $download_file ) ) :
						?>
						<a href="<?php echo esc_url( $download_file ); ?>" class="btn btn--xl btn--main">
							<span><?php esc_html_e( 'Download full version PDF', 'ostd' ); ?></span>
						</a>
					<?php
					endif;

					if ( ! empty( $header_btns ) ) :
						foreach ( $header_btns as $btn_item ) {
							if ( $btn_item['enable'] ) {
								switch ( $btn_item['type'] ) {
									case 'type1':
										?>
										<a
												href="<?php echo esc_url( $btn_item['btn_link'] ); ?>"
												class="btn btn--main btn--lg title-block__btn">
											<span><?php echo esc_html( $btn_item['btn_text'] ); ?></span>
										</a>
										<?php
										break;
									case 'type2':
										?>
										<a
												href="<?php echo esc_url( $btn_item['btn_link'] ); ?>"
												class="btn btn--secondary btn--lg title-block__btn">
											<span><?php echo esc_html( $btn_item['btn_text'] ); ?></span>
										</a>
										<?php
										break;
								}
							}
						}
					endif;
					?>
				</div>
				<?php
				if ( ! empty( $header_image['desktop'] ) ) :
					?>
					<div class="title-block__img ">
						<img src="<?php echo esc_url( $header_image['desktop'] ); ?>" alt="title-block-img">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<section class="cases-widget gradient-bg--secondary">
		<div class="container">
			<div class="cases-widget__wrap">
				<div class="cases-widget__left">
					<?php
					if ( ! empty( $cases ) ) :
						foreach ( $cases as $cases_item ) :
							?>
							<div class="cases-widget__item cases-widget-card">
								<div class="cases-widget-card__icon">
									<img
											src="<?php echo esc_url( $cases_item['image'] ); ?>"
											alt="cases-widget-card-icon">
								</div>
								<h4 class="heading-sm cases-widget-card__title"><?php echo esc_html( $cases_item['title'] ); ?></h4>
								<div class="body-text cases-widget-card__desc">
									<p>
										<?php echo esc_html( $cases_item['text'] ); ?>
									</p>
								</div>
							</div>
						<?php
						endforeach;
					endif;
					?>
					<?php if ( $testimonial_block['enable'] ) : ?>
						<div class="cases-widget__testimonials cases-widget-testim">
							<h5 class="heading-sm cases-widget-testim__title"><?php esc_html_e( 'Testimonials', 'ostd' ); ?></h5>
							<div class="body-text cases-widget-testim__desc">
								<p>
									<?php echo wp_kses_post( $testimonial_block['text'] ); ?>
								</p>
							</div>
							<div class="cases-widget-testim__info">
								<div class="cases-widget-testim__avatar">
									<img
											src="<?php echo esc_url( $testimonial_block['photo'] ); ?>"
											alt="cases-widget-testim-avatar">
								</div>
								<div class="cases-widget-testim__author">
									<p class="subtitle cases-widget-testim__company"><?php echo esc_html( $testimonial_block['name'] ); ?></p>
									<p class="body-text cases-widget-testim__name"><?php echo esc_html( $testimonial_block['position'] ); ?></p>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<div class="cases-widget__right">
					<div class="cases-widget__right-wrap">
						<h5 class="heading-sm"><?php echo esc_html( $about_client['title'] ); ?></h5>
						<p class="body-text cases-widget__right-text">
							<?php echo esc_html( $about_client['text'] ); ?>
						</p>
						<div class="cases-widget__proj-specs">
							<h5 class="heading-sm cases-widget__proj-title"><?php esc_html_e( 'Project Specs', 'ostd' ); ?></h5>
							<?php
							if ( ! empty( $project_specs ) ) :
								foreach ( $project_specs as $project_specs_item ) :
									?>
									<div class="cases-widget__proj-item">
										<p class="cases-widget__num"><?php echo esc_html( $project_specs_item['number'] ); ?></p>
										<p class="subtitle subtitle--main cases-widget__num-text">
											<?php echo esc_html( $project_specs_item['title'] ); ?>
										</p>
									</div>
								<?php
								endforeach;
							endif;
							?>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	<section class="services-widget main-bg">
		<div class="container">
			<div class="services-widget__wrap">
				<h2 class="heading-lg services-widget__title">
					Our Services
				</h2>
				<div class="services-widget__cards">
					<?php
					if ( ! empty( $services_block['service_items'] ) ) :
						foreach ( $services_block['service_items'] as $service_item ) :
							?>
							<div class="services-widget__card service-card">
								<div class="service-card__icon">
									<img src="<?php echo esc_url( $service_item['image'] ); ?>" alt="service-card-icon">
								</div>
								<h4 class="subtitle service-card__title"><?php echo esc_html( $service_item['title'] ); ?></h4>
								<div class="body-text service-card__desc">
									<p>
										<?php echo esc_html( $service_item['text'] ); ?>
									</p>
								</div>
								<div
										class="service-card__bottom-bar"
										style="background-color: <?php echo esc_attr( $service_item['color_background'] ); ?>;"></div>
							</div>
						<?php
						endforeach;
					endif;
					?>
				</div>
			</div>
		</div>
	</section>
	<?php
	get_template_part( 'template-parts/part-contact_form' );
	?>
</main>
<div class="news-letter" data-container="<?php echo esc_attr( $args['container'] ); ?>">
	<?php if ( empty( $args['container'] ) || $args['container'] !== 'false' ) : ?>
	<div class="container container--xxl">
		<?php endif; ?>
		<div class="news-letter__wrap">

			<div class="news-letter__text">
				<?php the_field( 'text_for_email_subscribe', 'option' ); ?>
			</div>
			<form name="js-news" class="news-letter-form">
				<label class="cust-input">
					<input
							type="hidden" name="field[email_address][name]"
							value="<?php esc_html_e( 'Email address', 'ostd' ); ?>">
					<input
							class="cust-input__input" type="email" required name="field[email_address][value]"
							placeholder="<?php esc_html_e( 'Email address *', 'ostd' ); ?>">
				</label>
				<button type="submit" class="btn btn--secondary news-letter-form__btn">
					<span><?php esc_html_e( 'Subscribe', 'ostd' ); ?></span>
				</button>
			</form>

		</div>
		<?php if ( empty( $args['container'] ) || $args['container'] !== 'false' ) : ?>
	</div>
<?php endif; ?>
</div>

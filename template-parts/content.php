<main class="main ">
	<section class="post">
		<section class="post__head gradient-bg">
			<div class="container container--lg">
				<?php
				if ( function_exists( 'my_breadcrumbs' ) ) {
					my_breadcrumbs();
				}
				?>
				<div class="heading post__title"><?php the_title(); ?></div>
				<p class="subtitle subtitle--main post__subtitle"><?php echo esc_html( get_field( 'subtitle' ) ); ?></p>
				<div class="post__info">
					<p class="body-text post-preview__author">
						<?php
						$cats = wp_get_post_categories( get_the_id(), [ 'fields' => 'all' ] );
						if ( ! empty( $cats ) ) {
							?>
							<span class="post-preview__cats">
							<?php
							foreach ( $cats as $cat ) {
								if ( 1 !== $cat->term_id ) {
									echo '<span class="post-preview__cat" style="background: ' . esc_attr( get_field( 'text_color', $cat ) ) . '">' . esc_html( $cat->name ) . '</span>';
								}
							}
							?>
							</span>
							<?php
						}
						?>
						<span class="post-preview__date">
							<?php
							esc_html_e( 'Updated ', 'ostd' );
							echo esc_html( get_the_date( 'M d' ) );
							?>
						</span>
						<?php if ( ! empty( get_field( 'post_author_name' ) ) ) : ?>
							<span><?php the_field( 'post_author_name' ); ?></span>
						<?php endif; ?>
					</p>
				</div>
			</div>
		</section>

		<section class="post__content">
			<div class="container container--xxl">
				<?php
				the_content();
				?>
			</div>
		</section>

		<?php
		if (
			! empty( get_field( 'post_author_avatar' ) )
			&& ! empty( get_field( 'post_author_name' ) )
			&& ! empty( get_field( 'post_author_description' ) )
		) :
			?>
			<section class="post-author">
				<div class="container container--xxl">
					<div class="post-author__row">
						<div class="post-author__avatar">
							<?php
							$image = get_field( 'post_author_avatar' );
							if ( ! empty( $image ) ) :
								?>
								<img
										src="<?php echo esc_url( $image['url'] ); ?>"
										alt="<?php echo esc_attr( $image['alt'] ); ?>"/>
							<?php endif; ?>
						</div>
						<div class="post-author__content">
							<span class="post-author__title">
								<?php the_field( 'post_author_title' ); ?>
							</span>
							<span><?php the_field( 'post_author_name' ); ?></span>
							<?php the_field( 'post_author_description' ); ?>
						</div>
					</div>
				</div>
			</section>
		<?php endif; ?>

	</section>
	<?php
	get_template_part( 'template-parts/part-contact_form' );
	?>
</main>

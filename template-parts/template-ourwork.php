<?php
/**
 * Header data
 *
 * @package OSTD
 */

$header_title         = get_field( 'header_title' );
$header_subtitle      = get_field( 'header_subtitle' );
$header_uptext        = get_field( 'header_uptext' );
$header_btn_read_more = get_field( 'header_btn_read_more' );
?>
<main class="main">
	<section class="title-block title-block--secondary ourWork__title-block">
		<div class="container">
			<div class="title-block__wrap">
				<?php if ( function_exists( 'my_breadcrumbs' ) ) : ?>
					<div class="flex-center"><?php my_breadcrumbs(); ?></div>
				<?php endif; ?>
				<div class="subtitle title-block__uptext"><?php echo esc_html( $header_uptext ); ?></div>
				<h1 class="heading title-block__title">
					<?php echo esc_html( $header_title ); ?>
				</h1>
				<div class="subtitle subtitle--main title-block__desc">
					<p>
						<?php echo wp_kses_post( $header_subtitle ); ?>
					</p>
				</div>
				<div class="cases">
					<div class="cases__wrap">
						<?php
						$cases = new WP_Query(
							[
								'post_type'      => 'cases',
								'posts_per_page' => 3,
							]
						);
						?>
						<div class="cases__items">
							<?php
							if ( $cases->have_posts() ) :
								while ( $cases->have_posts() ) :
									$cases->the_post();
									$id_cases = get_the_ID();
									?>
									<div class="cases__item case-item case-item--second">
										<div class="case-item__img">
											<img
													src="<?php echo esc_url( get_the_post_thumbnail_url( $id_cases ) ); ?>"
													alt="case-item-img"
											>
											<a
													href="<?php the_permalink(); ?>"
													class="btn btn--secondary case-item__img-btn">
												<span><?php esc_html_e( 'Learn more', 'ostd' ); ?></span>
											</a>
										</div>
										<h2 class="heading-sm case-item__title"><?php the_title(); ?></h2>
										<div class="body-text case-item__desc">
											<p>
												<?php echo wp_kses_post( get_field( 'except' ) ); ?>
											</p>
										</div>
										<a
												href="<?php the_permalink(); ?>"
												class="btn btn--secondary case-item__btn">
											<span><?php esc_html_e( 'Learn more', 'ostd' ); ?></span>
										</a>
									</div>
								<?php
								endwhile;
								wp_reset_postdata();
							endif;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php
	get_template_part( 'template-parts/part-contact_form' );
	?>
</main>

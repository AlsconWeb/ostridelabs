<?php
$contact_form_settings = get_field( 'contact_form_settings', url_to_postid( get_home_url() ) );
?>
<section class="contact" id="contact">
	<div class="container container--xxl">
		<div class="contact__wrap">
			<h2 class="heading-lg contact__title">
				<?php echo esc_html( $contact_form_settings['title'] ); ?>
			</h2>
			<div class="subtitle subtitle--main contact__desc">
				<p><?php echo esc_html( $contact_form_settings['subtitle'] ); ?></p>
			</div>
			<form class="contact__form contact-form" name="js-form">
				<div class="contact-form__col">
					<?php
					$checked = true;
					if ( ! empty( $contact_form_settings['contact_form_subjects'] ) ) :
						foreach ( $contact_form_settings['contact_form_subjects'] as $subject_item ) {
							?>
							<label class="contact-form__radio contact-form-radio">
								<input type="hidden" name="field[subject][name]" value="Subject">
								<input
										type="radio" <?php echo $checked ? 'checked' : ''; ?>
										name="field[subject][value]"
										value="<?php echo esc_attr( $subject_item['title'] ); ?>">
								<div class="contact-form-radio__wrap">
									<div class="contact-form-radio__mark"></div>
									<h3 class="heading-xs contact-form-radio__title">
										<?php echo esc_html( $subject_item['title'] ); ?>
									</h3>
									<div class="body-text contact-form-radio__desc">
										<p>
											<?php echo esc_html( $subject_item['text'] ); ?>
										</p>
									</div>
								</div>
							</label>
							<?php
							$checked = false;
						}
					endif;
					?>
				</div>
				<div class="contact-form__col">
					<div class="contact-form__body">
						<h3 class="heading-xs"><?php esc_html_e( 'Contact form', 'ostd' ); ?></h3>
						<h3 class="heading-xs"
							id="empty-message"><?php esc_html_e( 'Please fill in the empty field!', 'ostd' ); ?></h3>
						<label class="cust-input contact-form__field">
							<input
									type="hidden" name="field[full_name][name]"
									value="<?php esc_html_e( 'Full name', 'ostd' ); ?>">
							<input
									class="cust-input__input" type="text" name="field[full_name][value]"
									placeholder="<?php esc_html_e( 'Full name (optional)', 'ostd' ); ?>">
						</label>
						<label class="cust-input  contact-form__field">
							<input
									type="hidden" name="field[company_name][name]"
									value="<?php esc_html_e( 'Company name', 'ostd' ); ?>">
							<input
									class="cust-input__input" type="text" name="field[company_name][value]"
									placeholder="<?php esc_html_e( 'Company name (optional)', 'ostd' ); ?>">
						</label>
						<label class="cust-input  contact-form__field">
							<input
									type="hidden" name="field[email_address][name]"
									value="<?php esc_html_e( 'Email address', 'ostd' ); ?>">
							<input
									class="cust-input__input" type="email" required name="field[email_address][value]"
									placeholder="<?php esc_html_e( 'Email address *', 'ostd' ); ?>">
						</label>
						<label class="cust-input cust-input--area contact-form__field contact-form__field--area">
							<input
									type="hidden" name="field[message][name]"
									value="<?php esc_html_e( 'Message', 'ostd' ); ?>">
							<textarea
									class="cust-input__input" name="field[message][value]"
									placeholder="<?php esc_html_e( 'Message (optional)', 'ostd' ); ?>">
							</textarea>
						</label>
						<button type="submit" class="btn btn--secondary contact-form__btn">
							<span><?php esc_html_e( 'SEND MESSAGE', 'ostd' ); ?></span>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>

<?php
/**
 * Header data
 *
 * @package OSTD
 */

$header_title    = get_field( 'header_title' );
$header_subtitle = get_field( 'header_subtitle' );
$header_image    = get_field( 'header_image' );
$header_btns     = get_field( 'header_btns' ) ?: [];
?>
<main class="main">
	<section class="title-block title-block--fourth blog__title-block gradient-bg blog-page">
		<div class="container">
			<div class="title-block__wrap">
				<div class="title-block__inner">
					<h1 class="heading-lg title-block__title">
						<?php echo esc_html( $header_title ); ?>
					</h1>
					<?php
					if ( function_exists( 'my_breadcrumbs' ) ) {
						my_breadcrumbs();
					}
					?>
				</div>

				<div class="title-block__btns">
					<?php
					if ( ! empty( $header_btns ) ) :
						foreach ( $header_btns as $btn_item ) {
							if ( $btn_item['enable'] ) {
								switch ( $btn_item['type'] ) {
									case 'type1':
										?>
										<a
												href="<?php echo esc_url( $btn_item['btn_link'] ); ?>"
												class="btn btn--main btn--lg title-block__btn">
											<span><?php echo esc_html( $btn_item['btn_text'] ); ?></span>
										</a>
										<?php
										break;
									case 'type2':
										?>
										<a
												href="<?php echo esc_url( $btn_item['btn_link'] ); ?>"
												class="btn btn--secondary btn--lg title-block__btn">
											<span><?php echo esc_html( $btn_item['btn_text'] ); ?></span>
										</a>
										<?php
										break;
								}
							}
						}
					endif;
					?>
				</div>
				<div class="title-block__img ">
					<img
							class="desk" src="<?php echo esc_url( $header_image['desktop'] ); ?>"
							alt="title-block-img-desk">
					<img
							class="mobile"
							src="<?php echo esc_url( $header_image['mobile'] ); ?>"
							alt="title-block-img-mobile">
				</div>
			</div>
		</div>
	</section>
	<?php get_template_part( 'template-parts/block/news-letter' ); ?>
	<section class="blog__posts">
		<div class="d-none">
			<svg
					id="upload" width="24" height="24" viewBox="0 0 24 24" stroke="currentcolor" fill="none"
					xmlns="http://www.w3.org/2000/svg">
				<path
						d="M8.55254 7.28817L10.8747 4.96603V17.25C10.8747 17.5484 10.9932 17.8346 11.2042 18.0455C11.4152 18.2565 11.7013 18.375 11.9997 18.375C12.298 18.375 12.5842 18.2565 12.7952 18.0455C13.0061 17.8346 13.1247 17.5484 13.1247 17.25V4.96603L15.4468 7.28817C15.5512 7.3931 15.6752 7.47641 15.8118 7.53332C15.9484 7.59023 16.0949 7.61963 16.2429 7.61984C16.3909 7.62004 16.5375 7.59104 16.6743 7.53449C16.811 7.47795 16.9353 7.39498 17.04 7.29033C17.1446 7.18568 17.2276 7.06142 17.2841 6.92465C17.3407 6.78788 17.3697 6.6413 17.3695 6.49331C17.3693 6.34531 17.3399 6.19881 17.2829 6.0622C17.226 5.92559 17.1427 5.80154 17.0378 5.69718L12.7952 1.45453C12.6907 1.35005 12.5667 1.26718 12.4302 1.21064C12.2937 1.1541 12.1474 1.125 11.9997 1.125C11.8519 1.125 11.7056 1.1541 11.5691 1.21064C11.4326 1.26718 11.3086 1.35005 11.2042 1.45453L6.96155 5.69718C6.85662 5.80154 6.77331 5.92559 6.7164 6.0622C6.65949 6.19881 6.63009 6.34531 6.62988 6.49331C6.62968 6.6413 6.65868 6.78788 6.71523 6.92465C6.77177 7.06142 6.85474 7.18568 6.95939 7.29033C7.06404 7.39498 7.1883 7.47795 7.32507 7.53449C7.46184 7.59104 7.60842 7.62004 7.75641 7.61984C7.90441 7.61963 8.05091 7.59023 8.18752 7.53332C8.32413 7.47641 8.44817 7.3931 8.55254 7.28817Z"
						fill="currentcolor" stroke="white" stroke-width="0.5"/>
				<path
						d="M21.75 10.875C21.4516 10.875 21.1655 10.9935 20.9545 11.2045C20.7435 11.4155 20.625 11.7016 20.625 12V20.625H3.375V12C3.375 11.7016 3.25647 11.4155 3.0455 11.2045C2.83452 10.9935 2.54837 10.875 2.25 10.875C1.95163 10.875 1.66548 10.9935 1.4545 11.2045C1.24353 11.4155 1.125 11.7016 1.125 12V21C1.125 21.4973 1.32254 21.9742 1.67417 22.3258C2.02581 22.6775 2.50272 22.875 3 22.875H21C21.4973 22.875 21.9742 22.6775 22.3258 22.3258C22.6775 21.9742 22.875 21.4973 22.875 21V12C22.875 11.7016 22.7565 11.4155 22.5455 11.2045C22.3345 10.9935 22.0484 10.875 21.75 10.875Z"
						fill="currentcolor" stroke="white" stroke-width="0.5"/>
			</svg>
		</div>
		<div class="container container--lg">
			<div class="blog__posts-wrap">
				<?php
				$news        = new WP_Query(
					[
						'post_type' => 'post',
						'paged'     => get_query_var( 'paged' ) ?: 1,
					]
				);
				$total_pages = $news->max_num_pages;
				?>

				<?php
				if ( $news->have_posts() ) {
					while ( $news->have_posts() ) {
						$news->the_post();
						?>
						<a href="<?php the_permalink(); ?>">
							<div class="blog__post post-preview">
								<?php
								if ( get_the_post_thumbnail_url() ) {
									$width = '';
									?>
									<div class="img_blog_post">
										<img
												src="<?php echo esc_url( get_the_post_thumbnail_url() ); ?>"
												alt="img-blog-post">
									</div>
									<?php
								} else {
									$width = 'style="width: 100%"';
								}
								?>
								<div class="text_blog_post" <?php echo esc_attr( $width ); ?>>
									<h3 class="heading-lg post-preview__title"><?php the_title(); ?></h3>
									<p class="body-text post-preview__author">
										<?php
										$cats = wp_get_post_categories( get_the_id(), [ 'fields' => 'all' ] );
										if ( ! empty( $cats ) ) {
											?>
											<span class="post-preview__cats">
											<?php
											foreach ( $cats as $cat ) {
												if ( 1 !== $cat->term_id ) {
													echo '<span class="post-preview__cat" style="background-color: ' . esc_attr( get_field( 'text_color', $cat ) ) . '">' . esc_html( $cat->name ) . '</span>';
												}
											}
											?>
											</span>
											<?php
										}
										?>
										<span class="post-preview__date">
											<?php
											esc_html_e( 'Updated ', 'ostd' );
											echo get_the_date( 'M d' );
											?>
										</span>
										<?php if ( ! empty( get_field( 'post_author_name' ) ) ) : ?>
											<span><?php the_field( 'post_author_name' ); ?></span>
										<?php endif; ?>

									</p>
									<p class="post-preview__subtitle subtitle subtitle--main">
										<?php echo esc_html( get_field( 'subtitle' ) ); ?>
									</p>

									<div class="post-preview__content">
										<p class="subtitle post-preview__content-title"><?php echo esc_html( get_field( 'except_title' ) ); ?></p>
										<div class="body-text post-preview__text">
											<p>
												<?php echo esc_html( get_field( 'except' ) ); ?>
											</p>
										</div>
									</div>
									<div class="post-preview__footer">
										<p class="body-text post-preview__link"><?php echo esc_html_e( 'Read More', 'ostd' ); ?></p>
									</div>
								</div>

							</div>
						</a>
						<?php
					}
					wp_reset_postdata();
				}
				?>
			</div>
			<?php
			if ( $total_pages > 1 ) {
				$current_page = max( 1, get_query_var( 'paged' ) );
				$args         = [
					'total'     => $total_pages,
					'current'   => $current_page,
					'prev_next' => false,
					'prev_text' => '',
					'next_text' => '',
					'type'      => 'list',
				];

				echo wp_kses_post( paginate_links( $args ) );
			}
			?>
		</div>
	</section>
	<?php
	get_template_part( 'template-parts/part-contact_form' );
	?>
</main>

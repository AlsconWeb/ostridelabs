<?php
/**
 * Header data
 *
 * @package OSTD
 */

$header_title    = get_field( 'header_title' );
$header_subtitle = get_field( 'header_subtitle' );
$header_image    = get_field( 'header_image' );
$header_btns     = get_field( 'header_btns' );

/** Page data */
$text_block   = get_field( 'text_block' );
$career_email = get_field( 'email_for_career', url_to_postid( get_home_url() ) );

?>

<main class="main">
	<section class="title-block title-block--third career__title-block gradient-bg">
		<div class="container">
			<div class="title-block__wrap">
				<?php my_breadcrumbs(); ?>
				<h1 class="heading title-block__title">
					<?php echo esc_html( $header_title ); ?>
				</h1>
				<div class="subtitle subtitle--main title-block__subtitle">
					<p>
						<?php echo esc_html( $header_subtitle ); ?>
					</p>
				</div>
				<div class="title-block__img ">
					<img
							class="desk" src="<?php echo esc_url( $header_image['desktop'] ); ?>"
							alt="title-block-img-desk">
					<img
							class="mobile" src="<?php echo esc_url( $header_image['mobile'] ); ?>"
							alt="title-block-img-mobile">
				</div>
				<div class="title-block__btns">
					<?php
					if ( ! empty( $header_btns ) ) :
						foreach ( $header_btns as $btn_item ) {
							if ( $btn_item['enable'] ) {
								switch ( $btn_item['type'] ) {
									case 'type1':
										?>
										<a
												href="<?php echo esc_url( $btn_item['btn_link'] ); ?>"
												class="btn btn--main btn--lg title-block__btn">
											<span><?php echo esc_html( $btn_item['btn_text'] ); ?></span>
										</a>
										<?php
										break;
									case 'type2':
										?>
										<a
												href="<?php echo esc_url( $btn_item['btn_link'] ); ?>"
												class="btn btn--secondary btn--lg title-block__btn">
											<span><?php echo esc_html( $btn_item['btn_text'] ); ?></span>
										</a>
										<?php
										break;
								}
								?>
								<?php
							}
						}
					endif;
					?>
				</div>
			</div>
		</div>
	</section>
	<section class="opportunities">
		<?php
		$vacancy   = new WP_Query(
			[
				'post_type'      => 'job_vacancy',
				'posts_per_page' => - 1,
			]
		);
		$vacancies = [];
		if ( $vacancy->have_posts() ) {
			while ( $vacancy->have_posts() ) {
				$vacancy->the_post();
				$vacancies[ get_the_ID() ] = [
					'title'        => get_the_title(),
					'time'         => get_field( 'employment_time' ),
					'requirements' => array_map(
						function ( $item ) {
							return $item['text'];
						},
						get_field( 'requirements' )
					),
				];
			}
			wp_reset_postdata();
		}
		?>
		<script>
			let opportunitiesItems = JSON.parse( '<?php echo wp_json_encode( $vacancies ); ?>' );
		</script>
		<div class="container container--lg">
			<div class="opportunities__wrap">
				<h2 class="heading-lg opportunities__title">
					<?php the_field( 'opportunities_title' ); ?>
				</h2>

				<div class="opportunities__items">
					<?php
					if ( $vacancies ) :
						foreach ( $vacancies as $key => $vacancy_item ) {
							?>
							<div class="opportunities__item opportunity-item js-toggleActive-el">
								<h3 class="heading-xs opportunity-item__title"><?php echo esc_html( $vacancy_item['title'] ); ?></h3>
								<div class="opportunity-item__time">
									<div class="icon">
										<svg
												width="24" height="24" viewBox="0 0 24 24" fill="none"
												xmlns="http://www.w3.org/2000/svg">
											<path
													d="M11.99 2C6.47 2 2 6.48 2 12C2 17.52 6.47 22 11.99 22C17.52 22 22 17.52 22 12C22 6.48 17.52 2 11.99 2ZM12 20C7.58 20 4 16.42 4 12C4 7.58 7.58 4 12 4C16.42 4 20 7.58 20 12C20 16.42 16.42 20 12 20Z"
													fill="#595959"/>
											<path d="M12.5 7H11V13L16.25 16.15L17 14.92L12.5 12.25V7Z" fill="#595959"/>
										</svg>
									</div>
									<p class="text"><?php echo esc_html( $vacancy_item['time'] ); ?></p>
								</div>
								<button type="button" class="btn btn--md btn--secondary desk opportunity-item__btn"
										data-opPopup="<?php echo esc_attr( $key ); ?>">
									<span><?php esc_html_e( 'Learn more', 'ostd' ); ?></span>
								</button>
								<button
										type="button"
										class="btn btn--md btn--secondary mobile opportunity-item__btn js-toggleActive">
									<span><?php esc_html_e( 'Learn more', 'ostd' ); ?></span>
								</button>
								<div class="opportunity-item__hidden">
									<button type="button" class="opportunity-item__close js-toggleActive"></button>
									<h4 class="subtitle subtitle--main"><?php esc_html_e( 'Requirements', 'ostd' ); ?></h4>
									<ul class="cust-list opportunity-item__hidden-list">
										<?php
										if ( $vacancy_item['requirements'] ) {
											foreach ( $vacancy_item['requirements'] as $requirement_item ) {
												echo '<li>' . esc_html( $requirement_item ) . '</li>';
											}
										}
										?>
									</ul>
								</div>
							</div>
							<?php
						}
					endif;
					?>
				</div>
				<div class="opportunities__footer">
					<div class="body-text opportunities__footer-text">
						<p>
							<?php the_field( 'opportunities_text' ); ?>
						</p>
					</div>
					<a href="mailto:<?php echo esc_attr( $career_email ); ?>" class="opportunities__email">
                        <span class="icon">
                            <svg
									width="24" height="24" viewBox="0 0 24 24" fill="none"
									xmlns="http://www.w3.org/2000/svg">
                                <path
										fill="none"
										d="M4 4H20C21.1 4 22 4.9 22 6V18C22 19.1 21.1 20 20 20H4C2.9 20 2 19.1 2 18V6C2 4.9 2.9 4 4 4Z"
										stroke="currentcolor" stroke-width="2" stroke-linecap="round"
										stroke-linejoin="round"/>
                                <path
										fill="none" d="M22 6L12 13L2 6" stroke="currentcolor" stroke-width="2"
										stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </span>
						<span class="text"><?php echo esc_html( $career_email ); ?></span>
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="take-challenge third-bg">
		<div class="container">
			<h2 class="heading-lg take-challenge__title"><?php echo esc_html( $text_block['title'] ); ?></h2>
			<div class="subtitle subtitle--main take-challenge__subtitle">
				<p>
					<?php echo wp_kses_post( $text_block['text'] ); ?>
				</p>
			</div>
		</div>
	</section>
	<?php
	get_template_part( 'template-parts/part-contact_form' );
	?>
</main>

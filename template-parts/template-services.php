<?php
/**
 * Header data
 *
 * @package OSTD
 */

$header_title         = get_field( 'header_title' ) ?? '';
$header_subtitle      = get_field( 'header_subtitle' ) ?? '';
$header_up_text       = get_field( 'header_uptext' ) ?? '';
$header_btn_read_more = get_field( 'header_btn_read_more' ) ?? '';

/** Page data */
$services_block        = get_field( 'services_block' ) ?? '';
$services_action_block = get_field( 'services_action_block' ) ?? '';

?>

<main class="main">
	<section class="title-block title-block--secondary services__title-block main-bg">
		<div class="container">
			<div class="title-block__wrap">
				<div class="title-block__img">
					<img
							src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/svg/title-img.svg' ); ?>"
							alt="title-block-img"
					>
				</div>
				<?php if ( function_exists( 'my_breadcrumbs' ) ) : ?>
					<div class="flex-center"><?php my_breadcrumbs(); ?></div>
				<?php endif; ?>
				<h1 class="heading title-block__title">
					<?php echo esc_html( $header_title ); ?>
				</h1>
				<div class="subtitle subtitle--main title-block__desc">
					<p>
						<?php echo esc_html( $header_subtitle ); ?>
					</p>
				</div>
				<div class="services-widget">
					<div class="services-widget__wrap">
						<div class="services-widget__cards">
							<?php
							if ( ! empty( $services_block['service_items'] ) ) :
								foreach ( $services_block['service_items'] as $service_item ) :
									?>
									<a href="<?php echo esc_url( $service_item['link'] ); ?>">
										<div class="services-widget__card service-card">
											<div class="service-card__icon">
												<img
														src="<?php echo esc_url( $service_item['image'] ); ?>"
														alt="service-card-icon">
											</div>
											<h3 class="subtitle service-card__title"><?php echo esc_html( $service_item['title'] ); ?></h3>
											<div class="body-text service-card__desc">
												<p>
													<?php echo esc_html( $service_item['text'] ); ?>
												</p>
											</div>
											<div
													class="service-card__bottom-bar"
													style="background-color: <?php echo esc_attr( $service_item['color_background'] ); ?>;">
											</div>
										</div>
									</a>
								<?php
								endforeach;
							endif;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="services-exp">
		<div class="container container--xxl">
			<div class="services-exp__wrap">
				<?php
				if ( $services_action_block ) :
					foreach ( $services_action_block as $service_action_item ) :
						?>
						<div
								class="services-exp__card services-exp-card"
								id="<?php echo esc_attr( $service_action_item['block_id'] ); ?>">
							<div class="services-exp-card__content">
								<h2 class="heading-lg services-exp-card__title">
									<?php echo esc_html( $service_action_item['title'] ); ?>
								</h2>
								<div class="subtitle subtitle--main services-exp-card__subtitle">
									<p>
										<?php echo esc_html( $service_action_item['text'] ); ?>
									</p>
								</div>
								<?php echo wp_kses_post( $service_action_item['content'] ); ?>
								<a href="#contact" class="btn btn--secondary btn--xl services-exp-card__btn">
									<span><?php esc_html_e( 'Request a free consultation', 'ostd' ); ?></span>
								</a>
							</div>
							<div class="services-exp-card__img">
								<img
										src="<?php echo esc_url( $service_action_item['image'] ); ?>"
										alt="services-exp-card-img">
							</div>
						</div>
					<?php
					endforeach;
				endif;
				?>
			</div>
		</div>
	</section>
	<section class="cases">
		<div class="container">
			<div class="cases__wrap">
				<h2 class="heading-lg cases__title">
					<?php the_field( 'our_cases_title' ); ?>
				</h2>
				<?php
				$cases = new WP_Query(
					[
						'post_type'      => 'cases',
						'posts_per_page' => 3,
					]
				);
				?>
				<div class="cases__items">
					<?php
					if ( $cases->have_posts() ) :
						while ( $cases->have_posts() ) :
							$cases->the_post();
							$id_cases = get_the_ID();
							?>
							<div class="cases__item case-item">
								<a href="<?php the_permalink(); ?>" class="case-item__img">
									<img
											src="<?php echo esc_url( get_the_post_thumbnail_url( $id_cases ) ); ?>"
											alt="case-item-img"
									>
								</a>
								<h4 class="heading-sm case-item__title"><?php the_title(); ?></h4>
								<div class="body-text case-item__desc">
									<p>
										<?php echo wp_kses_post( get_field( 'except' ) ); ?>
									</p>
								</div>
								<a href="<?php the_permalink(); ?>" class="btn btn--secondary case-item__btn">
									<span><?php esc_html_e( 'Learn more', 'ostd' ); ?></span>
								</a>
							</div>
						<?php
						endwhile;
						wp_reset_postdata();
					endif;
					?>
				</div>
			</div>
		</div>
	</section>
	<?php
	get_template_part( 'template-parts/part-contact_form' );
	?>
</main>
